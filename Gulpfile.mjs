/* eslint-disable no-undef, no-unused-vars */
/**
 * Require our dependencies
 */
import { dest, lastRun, parallel, series, src, watch, task } from 'gulp';
import autoprefixer from 'autoprefixer';
import babel from 'gulp-babel';
import browserSync from 'browser-sync';
import eslint from 'gulp-eslint';
import theo from 'theo';
import gulpTheo from 'gulp-theo';
import theoTemplate from './theo.config.mjs';
import header from 'gulp-header';
import nano from 'cssnano';
import postcss from 'gulp-postcss';
import rename from 'gulp-rename';
import * as dartSass from 'sass';

import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import uglify from 'gulp-uglify';
import stylelint from 'stylelint';

import headerTemplate from './_src/headers/header.mjs';
const paths = {
  scripts: '_src/js/**.js',
  src: '_src',
  css: 'static/css',
  js: 'static/js',
  scss: '_src/scss',
  tokens: '_src/tokens',
};

/**
 * Setup what files to watch and what tasks to run
 */
function watchFiles() {
  watch(paths.scss, series(lint, styles));
  watch(paths.scripts, scripts);
  watch(paths.tokens, series(tokens, lint, styles));
}

/**
 * Transform token json files to scss files
 */
function tokens() {
  theo.registerFormat('scss', `${theoTemplate}`);
  // Transform tokens/props.json to scss/props.scss:
  return src(`_src/tokens/*.json`)
    .pipe(
      gulpTheo({
        transform: { type: 'web' },
        format: { type: 'scss' },
      }),
    )
    .pipe(dest('_src/scss/tokens'));
}

/**
 * Lint, compile, and optimize our scripts
 */
function scripts(cb) {
  src(paths.scripts, { since: lastRun(scripts) })
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(babel())
    .pipe(uglify({ mangle: false }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest(`${paths.js}`))
    .pipe(browserSync.stream());

  cb();
}

/**
 * Lint our scss files
 */
async function lint() {
  const lintingOptions = {
    files: '_src/scss/**/*.scss',
    formatter: 'string',
  };
  (async () => {
    const results = await stylelint.lint(lintingOptions);
    console.error(results.report);
  })();
}

/**
 * Compile, and optimize style.css
 */
function styles() {
  // Settings
  const sassOptions = {
    errLogToConsole: true,
    includePaths: ['node_modules'],
    silenceDeprecations: [
      'legacy-js-api',
      'mixed-decls',
      'import',
      'global-builtin',
    ],
  };
  const postCSSPlugins = [autoprefixer, nano];
  return src('./_src/scss/**/*.scss')
    .pipe(sass.sync(sassOptions).on('error', sass.logError))
    .pipe(postcss(postCSSPlugins))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(dest(`${paths.css}`));
}

/**
 * Add package version number to the beginning of files
 */
function addHeaders() {
  return src(['./static/**/*.css'])
    .pipe(header(headerTemplate))
    .pipe(dest('./static'));
}

/**
 * Move files for release
 */
function moveFiles(cb) {
  src('CHANGELOG.md').pipe(rename('changelog.md')).pipe(dest('./_src/docs'));
  src('_src/scss/**/*.scss').pipe(dest('./scss'));
  src('static/css/**/*.css').pipe(header(headerTemplate)).pipe(dest('./css'));
  src('static/js/**/*.js').pipe(header(headerTemplate)).pipe(dest('./js'));
  cb();
}

/**
 * Create folder to upload to assets.tcu.edu/cornerstone
 */
function assets(cb) {
  return src(['static/js/**/*', 'static/css/**/*', '!static/css/tokens/**'], {
    base: './static',
  })
    .pipe(header(headerTemplate))
    .pipe(
      dest(`assets.tcu.edu/cornerstone/${process.env.npm_package_version}/`),
    );
}

/*
 * Configure a Fractal instance.
 *
 */
import fractal from './fractal.config.js';
const logger = fractal.cli.console; // keep a reference to the fractal CLI console utility

/*
 * Start the Fractal server
 *
 * In this example we are passing the option 'sync: true' which means that it will
 * use BrowserSync to watch for changes to the filesystem and refresh the browser automatically.
 * Obviously this is completely optional!
 *
 * This task will also log any errors to the console.
 */

function fractalStart() {
  const server = fractal.web.server({
    sync: true,
  });
  watchFiles();
  server.on('error', (err) => logger.error(err.message));
  return server.start().then(() => {
    logger.success(`Fractal server is now running at ${server.url}`);
  });
}

/*
 * Run a static export of the project web UI.
 *
 * This task will report on progress using the 'progress' event emitted by the
 * builder instance, and log any errors to the terminal.
 *
 * The build destination will be the directory specified in the 'builder.dest'
 * configuration option set in fractal.js.
 */

function fractalBuild() {
  const builder = fractal.web.builder();
  builder.on('progress', (completed, total) =>
    logger.update(`Exported ${completed} of ${total} items`, 'info'),
  );
  builder.on('error', (err) => logger.error(err.message));
  return builder.build().then(() => {
    logger.success('Fractal build completed!');
  });
}

task('lint', series(lint));
task('styles', series(tokens, 'lint', styles));
task('scripts', scripts);
task('files', parallel('styles', 'scripts'));
task('release', series(tokens, 'files', moveFiles, fractalBuild, assets));
task('fractal:build', series('files', moveFiles, fractalBuild));

task('fractal:start', series(fractalStart));
task('default', series('files', fractalStart));
