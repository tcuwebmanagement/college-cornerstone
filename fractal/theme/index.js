/* eslint-disable no-undef */
'use strict';

const mandelbrot = require('@frctl/mandelbrot');

/*
 * Configure the theme
 */
const subTheme = mandelbrot({
  styles: [
    'default',
    'https://fonts.googleapis.com/css?family=Oswald%7CSource+Sans+Pro:400,700%7CPlayfair+Display:400,700&display=swap',
    '/subtheme/css/style.css',
  ],
  favicon: '/subtheme/favicon.ico',
  nav: ['search', 'docs', 'components', 'information'],
  panels: ['notes', 'html', 'resources', 'info'],
  information: [
    {
      label: 'Version',
      value: require('../../package.json').version,
    },
    {
      label: 'Built on',
      value: new Date(),
      type: 'time', // Outputs a <time /> HTML tag
      format: (value) => {
        return value.toLocaleDateString('en');
      },
    },
  ],
});

subTheme.addRoute(
  '/components/wordpress/:handle',
  {
    handle: 'wordpress',
    view: 'pages/components/wordpress.nunj',
  },
  getHandles,
);

let handles = null;

function getHandles(app) {
  app.components.on('updated', () => (handles = null));
  if (handles) {
    return handles;
  }
  handles = [];
  app.components.flatten().each((comp) => {
    handles.push(comp.handle);
    if (comp.variants().size > 1) {
      comp.variants().each((variant) => handles.push(variant.handle));
    }
  });
  handles = handles.map((h) => ({ handle: h }));
  return handles;
}

/*
 * Specify a template directory to override any view templates
 */
subTheme.addLoadPath(__dirname + '/views');

/*
 * Specify the static assets directory that contains the custom stylesheet.
 */
subTheme.addStatic(__dirname + '/dist', '/subtheme');

/*
 * Export the customised theme instance so it can be used in Fractal projects
 */
module.exports = subTheme;
