module.exports = {
  // Adding to the global context can be useful when running fractal locally
  // within a project. This can be done in either this file or by editing the
  // default.context object setting in fractal.config.js

  // For including scripts or stylesheets in the head of the component pages
  globalhead: '',
  // For including scripts or stylesheets in the body of the component pages
  globalbody: '',
};
