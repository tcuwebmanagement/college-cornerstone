module.exports = function (components) {
  return {
    // register custom filters
    filters: {
      // usage: {{ label|kebab }}
      kebab: function (str) {
        if (!str) return '';

        return str.toLowerCase().replace(/\s+/g, '-');
      },
      titlecase: function (str, split = ' ') {
        if (!str) return '';

        return str
          .toLowerCase()
          .split(split)
          .map(function (word) {
            return word.charAt(0).toUpperCase() + word.slice(1);
          })
          .join(' ');
      },
    },
    functions: {
      getComponents: function (str) {
        let ret = [];
        for (let component of components.flatten()) {
          // Iterate over the variants and check if their tags matches str
          // If no variants, component label sets to default
          // If the variant is a default, remove the '--default' from the handle
          for (let variant of component.variants()) {
            if (component._isHidden === false && variant.tags.includes(str)) {
              if (variant.handle.includes('--default')) {
                variant.handle = variant.handle.replace('--default', '');
              }
              // Add to return array
              ret.push(variant.toJSON());
            }
          }
        }
        return ret;
      },
    },
  };
};
