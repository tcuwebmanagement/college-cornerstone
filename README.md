# College Cornerstone

College Cornerstone is TCU's design system for college websites.

# Installation

## Install via NPM

`npm install bitbucket:tcuwebmanagement/college-cornerstone`

# What’s Included

```
_src_/
  |—— js/
      |—— tcu.accordions.js
  |—— scss/
      |—— components/
      |—— elements/
      |—— foundation/
      |—— objects/
      |—— plugins/
      |—— settings/
      |—— tokens/
      |—— tools/
      |—— utilities/
      |—— _fonts.scss
      |—— _shame.scss
      |—— style.scss
```

# Applying College Cornerstone to a Project

## 1. Include styles

How you include the styles from College Cornerstone depends on your framework and build process.
Here we include setup instructions for gulp, sass, and using college-cornerstone as an npm dependency.

If you are using gulp-sass which makes use of node-sass, update your (includePath)[https://github.com/sass/node-sass#includepaths] to include the node_modules directory.

```
  // Gulpfile.js
  const sass = require('gulp-sass');

  return src(['./_src/scss/style.scss'])
    .pipe(sass({
      includePaths: ['node_modules/@tcu/college-cornerstone/_src/scss', 'node_modules']
    }).on('error', sass.logError))
```

From there, you can @import any of the files in `@tcu/college-cornerstone/_src/scss` within your project. For starting out, use `_src/scss/style.scss`.

## 2. Include fonts

```
<link href="https://fonts.googleapis.com/css?family=Oswald%7CSource+Sans+Pro:400,700%7CPlayfair+Display:400,700&display=swap" rel="stylesheet">
```

# Using Components

Components are included as twig files. How you include them depends on your framework and build process. Refer to the included component config files to see how the data should be structured to work with the twig files.

# Configuration

# Resources

# Working with college-cornerstone locally

## Working on components within college-cornerstone

1. Install the node modules required by the project.

   ```
   npm install
   ```

2. Run the default gulp task

   ```
   gulp
   ```

When adding new components, follow this structure in the `_src/components` directory. Run `npx plop component` for automated setup of component files.

```
components/     # Patterns
├── base/       # …that are basic building blocks like HTML tags
├── common/     # …that may appear anywhere
├── custom/     # …that may appear on specific pages
├── global/     # …that appear on every page
├── templates/  # …that combine patterns to render page types
└── utilities/  # …helpers that might not have visual representation
```

## Working on a site repo with a local version of college-cornerstone

If you want to see how your local changes to college-cornerstone affect a real project, you can create a symlink of your local version of college-cornerstone or install a specific commit from Bitbucket.

### Local symlink

1. Get the path to your local college-cornerstone directory.
2. Navigate to the root of the project you want to work on.
3. Create a symlink to your local copy of college-cornerstone: `ln -s ~/path/to/college-cornerstone node_modules/@tcu/college-cornerstone`
4. Create a copy of theo.config.js in the root of your project
5. Update your project's Gulpfile.js to point to the copied theo.config.js. Make sure to not commit this change.

### Reference a commit from Bitbucket

1. Navigate to the root of the project you want to work on.
2. Run `npm install --no-save bitbucket:tcuwebmanagement/college-cornerstone#<commit hash>` and replace `<commit hash>` with the commit that you would like to use.
3. Check the `node_modules/@tcu/college-cornerstone` directory to verify that it's using the correct version of the repo.

## Creating a release of college-cornerstone

1. Follow the instructions to (create a release PR)[https://app.clickup.com/2259002/v/dc/24y1u-1092/24y1u-308?block=block-39b3501a-62d1-42b1-8371-6c259a9837f1]

2. Run `npm run build`

3. Upload the `build` directory to https://sandbox.dev.tcu.edu/cornerstone

4. Upload the new version directory from `assets.tcu.edu/cornerstone/` to https://assets.tcu.edu/cornerstone/

## Dependencies

- (Cypress)[https://www.cypress.io/]
- (Foundation)[https://get.foundation/sites/docs/]
- (Fractal)[https://fractal.build/]
- (Theo)[https://github.com/salesforce-ux/theo]

# Connect

## Submitting an issue

(Issue Form)[https://forms.clickup.com/f/24y1u-2500/58MOYN6TC60YDB5EV9]

## Submitting a pull request

(Pull Request documentation)[https://app.clickup.com/2259002/v/dc/24y1u-1092/24y1u-308]
