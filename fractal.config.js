/* eslint-disable no-undef */
'use strict';

/* Create a new Fractal instance and export it for use elsewhere if required */
const fractal = (module.exports = require('@frctl/fractal').create());

/* Set the title of the project */
fractal.set('project.title', 'Cornerstone');

/* Require the Twig adapter */
const twigSettings = require('./fractal/twig.js');
const twigAdapter = require('@frctl/twig')(twigSettings(fractal.components));
// first set docs engine
fractal.docs.engine(twigAdapter);

// then set components engine
fractal.components.engine(twigAdapter);
fractal.components.set('ext', '.twig');

/* Tell Fractal where the components will live */
fractal.components.set('path', __dirname + '/_src/components');

/* Change the 'Components' label to 'Patterns' */
fractal.components.set('label', 'Patterns');
fractal.components.set('title', 'Patterns');

/* Change the statuses */
fractal.components.set('statuses', {
  'not-ready': {
    label: 'Not Ready',
    description: 'Not ready to be used in production',
    color: '#FFCC00',
  },
  'in-progress': {
    label: 'In Progress',
    description: 'Currently working on this component',
    color: '#5AAFEE',
  },
  refactor: {
    label: 'Needs Refactoring',
    description: 'Can be used in production but needs refactoring',
    color: '#5AAFEE',
  },
  deprecated: {
    label: 'Deprecated',
    description: 'Should no longer be used in production',
    color: '#D0021B',
  },
  complete: {
    label: 'Complete',
    description: 'Ready to be used in production',
    color: '#90AC23',
  },
});
fractal.components.set('default.status', 'not-ready');

/* Setup default global context */
const globalContext = require('./fractal/context.js');
fractal.components.set('default.context', globalContext);

/* Tell Fractal where the documentation pages will live */
fractal.docs.set('path', __dirname + '/_src/docs');

/* Specify a directory of static assets */
fractal.web.set('static.path', __dirname + '/static');

/* Set the static HTML build destination */
fractal.web.set('builder.dest', __dirname + '/build');

/* BrowserSync Options */
fractal.web.set('server.syncOptions', {
  open: true,
  notify: true,
});

/**
 * Custom theme
 */
const theme = require('./fractal/theme');
fractal.web.theme(theme);
