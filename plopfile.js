module.exports = function (plop) {
  // create your generators here
  plop.setGenerator('component', {
    description: 'Creates all necessary files for starting a new component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the name of your component?',
      },
      {
        type: 'list',
        name: 'type',
        choices: [
          {
            name: 'base - basic building blocks like HTML tags',
            value: 'base',
          },
          {
            name: 'common - may appear anywhere',
            value: 'common',
          },
          {
            name: 'custom - appear on specific sites',
            value: 'custom',
          },
          {
            name: 'global - appear on every page',
            value: 'global',
          },
          {
            name: 'layout - combine patterns to render page types',
            value: 'layout',
          },
        ],
        message: 'What type of component is it?',
      },
      {
        type: 'list',
        name: 'component-type',
        choices: [
          {
            name: 'none',
            value: '',
          },
          {
            name: 'component',
            value: 'ou-component',
          },
          {
            name: 'global',
            value: 'global',
          },
          {
            name: 'snippet',
            value: 'ou-snippet',
          },
          {
            name: 'system',
            value: 'ou-system',
          },
          {
            name: 'block',
            value: 'wordpress-block',
          },
        ],
        message: 'Which tag should be added to the component?',
      },
    ], // array of inquirer prompts
    actions: [
      {
        type: 'add',
        path: '_src/components/{{type}}/{{kebabCase name}}/{{kebabCase name}}.config.json',
        templateFile: 'plop-templates/component-config-js.hbs',
      },
      {
        type: 'add',
        path: '_src/components/{{type}}/{{kebabCase name}}/{{kebabCase name}}.twig',
      },
      {
        type: 'add',
        path: '_src/components/{{type}}/{{kebabCase name}}/README.md',
      },
      {
        type: 'add',
        path: '_src/scss/components/_{{kebabCase name}}.scss',
      },
    ], // array of actions
  });
};
