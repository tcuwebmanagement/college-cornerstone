/// <reference types="cypress" />

const WRAPPER = '.tcu-directory';

context('Results Listing - Grid View', () => {
  beforeEach(() => {
    cy.visit('/components/preview/results-listing');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});

context('Results Listing - List View', () => {
  beforeEach(() => {
    cy.visit('/components/preview/results-listing--list');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
