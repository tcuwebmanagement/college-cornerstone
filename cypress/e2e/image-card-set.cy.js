/// <reference types="cypress" />

const WRAPPER = 'body';

context('Image Card Set', () => {
  beforeEach(() => {
    cy.visit('/components/preview/image-card-set');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
