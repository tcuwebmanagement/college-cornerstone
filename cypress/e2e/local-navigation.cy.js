/// <reference types="cypress" />

const WRAPPER = '.tcu-local-nav';
const delay = 300;

context('Local Navigation', () => {
  beforeEach(() => {
    cy.visit('/components/preview/local-navigation');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  // Nav bar exists
  it('Local Navigation bar exists', () => {
    cy.get('.tcu-local-nav').should('exist');
  });

  // Nav bar has children
  it('Local Navigation bar has children', () => {
    cy.get('.tcu-local-nav .tcu-menu li').should('not.have.length', 0);
  });

  it('Mobile Local Navigation opens on click', () => {
    cy.viewport('iphone-6+');
    cy.wait(delay).get('.js-nav-toggle').click({ force: true });
    cy.get('.nav-wrapper').should('be.visible');
  });

  it('Mobile Local Navigation Child Nav opens on click', function () {
    cy.viewport('iphone-6+');
    cy.wait(delay).get('.js-nav-toggle > .tcu-button-arrow').click();
    cy.wait(delay)
      .get('.nav-wrapper')
      .find('.has-dropdown > button')
      .first()
      .click();
    cy.get('.nav-dropdown-open').should('not.have.length', 0).and('be.visible');
  });

  it('Desktop Local Navigation opens on click', () => {
    cy.viewport('macbook-13');
    cy.wait(delay)
      .get('.nav-wrapper')
      .find('.has-dropdown > button')
      .first()
      .click();
    cy.get('div[data-state="open"]')
      .should('not.have.length', 0)
      .and('be.visible');
  });

  it('Desktop Local Navigation Child Nav opens on click', function () {
    cy.viewport('macbook-13');
    cy.wait(delay)
      .get('.nav-wrapper')
      .find('.has-dropdown > button')
      .first()
      .click();
    cy.get('div[data-state="open"')
      .find('.has-dropdown > button')
      .first()
      .click();
    cy.get('.nav-dropdown-open').should('not.have.length', 0).and('be.visible');
  });
});
