/// <reference types="cypress" />

const WRAPPER = '.tcu-sidebar-menu';
const TITLE_WRAPPER = '.tcu-sidebar-menu__title-wrapper';
const ARROW = '.tcu-sidebar-menu__arrow';
const MENU_WRAPPER = '.tcu-sidebar-menu__wrapper';
const delay = 300;

/**
 * Check the visibility of the next sibling based on the parent's class.
 * This is due to the added functionality to only have the submenu of
 * the current page item open on load, the rest shpuld be closed.
 * @param {HTMLElement} $button - The button element being clicked.
 * @param {boolean} shouldBeVisible - Whether the next sibling should be visible.
 */
function checkNextSiblingVisibility($button, shouldBeVisible) {
  const parentElement = $button.parent();
  if (
    parentElement.hasClass('current-page-item') ||
    parentElement.hasClass('current-page-parent')
  ) {
    cy.wrap($button)
      .next()
      .should(shouldBeVisible ? 'be.visible' : 'not.be.visible');
  } else {
    cy.wrap($button)
      .next()
      .should(shouldBeVisible ? 'not.be.visible' : 'be.visible');
  }
}

context('Sidebar Menu', () => {
  beforeEach(() => {
    cy.visit('/components/preview/sidebar-menu');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  context('Mobile', () => {
    beforeEach(() => {
      cy.viewport('iphone-6+');
      cy.wait(delay);
    });

    it('Displays arrow button on mobile view', () => {
      cy.get(TITLE_WRAPPER).within(() => {
        cy.get(ARROW).should('be.visible');
      });
    });

    it('Sidebar wrapper visibility based on aria-expanded attribute', () => {
      cy.get(TITLE_WRAPPER).within(() => {
        cy.get(ARROW).as('arrowButton');
      });

      cy.get(MENU_WRAPPER).should('not.be.visible');
      cy.get('@arrowButton').click();
      cy.get(MENU_WRAPPER).should('be.visible');
      cy.get('@arrowButton').click();
      cy.get(MENU_WRAPPER).should('not.be.visible');
    });

    it('Sidebar Menu Submenus visibility based on aria-expanded attribute', () => {
      cy.get(TITLE_WRAPPER).within(() => {
        cy.get(ARROW).as('titleButton');
        cy.get('@titleButton').click();
      });

      cy.get(MENU_WRAPPER).within(() => {
        cy.get(ARROW).as('submenuButtons');
      });

      cy.get('@submenuButtons').then(($buttons) => {
        const buttonsCount = $buttons.length;

        // From last to first, next sibling (submenu items) should not be visible if parent contains current-page classes
        for (let i = buttonsCount - 1; i >= 0; i--) {
          cy.wrap($buttons[i])
            .click()
            .then(($button) => {
              checkNextSiblingVisibility($button, false);
            });
        }

        // From first to last, next sibling (submenu items) should be visible if parent does not contain current-page classes
        for (let i = 0; i < buttonsCount; i++) {
          cy.wrap($buttons[i])
            .click()
            .then(($button) => {
              checkNextSiblingVisibility($button, true);
            });
        }
      });
    });
  });
});
