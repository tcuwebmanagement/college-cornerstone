/// <reference types="cypress" />

const WRAPPER = '.tcu-lead-in-paragraph';

context('Lead In Paragraph', () => {
  beforeEach(() => {
    cy.visit('/components/preview/lead-in-paragraph');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
