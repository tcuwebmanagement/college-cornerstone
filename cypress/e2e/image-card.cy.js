/// <reference types="cypress" />

const WRAPPER = 'body';

context('Image Card', () => {
  beforeEach(() => {
    cy.visit('/components/preview/image-card');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
