/// <reference types="cypress" />

const WRAPPER = 'body';
const delay = 300;

context('Base Components', () => {
  /**
   * Buttons
   */
  describe('Buttons', () => {
    beforeEach(() => {
      cy.visit('/components/preview/buttons');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER, {
        rules: {
          region: { enabled: false },
        },
      });
    });
  });
  /**
   * Blockquote
   */
  describe('Blockquote', () => {
    beforeEach(() => {
      cy.visit('/components/preview/blockquote');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER, {
        rules: {
          region: { enabled: false },
        },
      });
    });
  });
  /**
   * Images
   */
  describe('Images', () => {
    beforeEach(() => {
      cy.visit('/components/preview/images');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER, {
        rules: {
          region: { enabled: false },
        },
      });
    });
  });
  /**
   * Tables
   */
  describe('Tables', () => {
    beforeEach(() => {
      cy.visit('/components/preview/tables');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER, {
        rules: {
          region: { enabled: false },
        },
      });
    });
  });
  /**
   * Separator
   */
  describe('Separator', () => {
    beforeEach(() => {
      cy.visit('/components/preview/separator');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER, {
        rules: {
          region: { enabled: false },
        },
      });
    });
  });
  /**
   * Typography
   */
  describe('Typography', () => {
    beforeEach(() => {
      cy.visit('/components/preview/all-typography');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER, {
        rules: {
          region: { enabled: false },
        },
      });
    });
  });
  /**
   * Video
   */
  describe('Video', () => {
    beforeEach(() => {
      cy.intercept('https://player.vimeo.com/api/player.js', (req) => {
        req.on('before:response', (res) => {
          res.headers['cache-control'] = 'no-store';
        });
      }).as('vimeo');
      cy.visit('/components/preview/video');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.wait('@vimeo').then((interception) => {
        // This is to help make sure vimeo has been initialized before running checkA11y
        cy.get('.tcu-video').should('have.descendants', 'iframe');
        cy.checkA11y(WRAPPER);
      });
    });

    it('Video button switches to pause', () => {
      cy.viewport('macbook-15');
      cy.wait('@vimeo').then((interception) => {
        cy.get('._pause').should('contain.text', 'Pause');
        cy.get('._pause').click();
        cy.get('._pause').should('contain.text', 'Play');
      });
    });

    it('Video button switches to play', () => {
      cy.viewport('macbook-15');
      cy.wait('@vimeo').then((interception) => {
        cy.get('._pause').should('contain.text', 'Pause');
        cy.get('._pause').click();
        cy.get('._pause').should('contain.text', 'Play');
        cy.get('._pause').click();
        cy.get('._pause').should('contain.text', 'Pause');
      });
    });
  });
});
