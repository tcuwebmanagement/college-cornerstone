/// <reference types="cypress" />

const WRAPPER = '.tcu-responsive-image';

context('Responsive Image', () => {
  beforeEach(() => {
    cy.visit('/components/preview/responsive-image');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
