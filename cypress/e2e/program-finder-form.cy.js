/// <reference types="cypress" />

const WRAPPER = '.tcu-program-finder-form';

context('Program Finder Form', () => {
  beforeEach(() => {
    cy.visit('/components/preview/program-finder-form');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
