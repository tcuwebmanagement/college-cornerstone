/// <reference types="cypress" />

const WRAPPER = '.tcu-alert';
const delay = 600;

context('Alert', () => {
  context('Announcement', () => {
    beforeEach(() => {
      cy.visit('/components/preview/alert--announcement');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.wait(delay);
      cy.checkAccessibility(WRAPPER);
    });
  });

  context('Warning', () => {
    beforeEach(() => {
      cy.visit('/components/preview/alert--warning');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.wait(delay);
      cy.checkAccessibility(WRAPPER);
    });
  });

  context('Emergency', () => {
    beforeEach(() => {
      cy.visit('/components/preview/alert--emergency');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.wait(delay);
      cy.checkAccessibility(WRAPPER);
    });
  });
});
