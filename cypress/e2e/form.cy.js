/// <reference types="cypress" />

const WRAPPER = '.ou-form';

context('Forms', () => {
  beforeEach(() => {
    cy.visit('/components/preview/form');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  it('Will not submit without required fields', () => {
    cy.get('#id_firstandlastname_test-form').type('Super Frog');
    cy.get('button[type="submit"]').click();
    cy.get('*:invalid').should('have.length', 4);
  });
});
