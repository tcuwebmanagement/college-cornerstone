/// <reference types="cypress" />

const WRAPPER = '.tcu-accordion-container';
const delay = 300;

context('Accordion', () => {
  beforeEach(() => {
    cy.visit('/components/preview/accordion--default');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  it('Accordion closes on click', function () {
    cy.get(':nth-child(1) > .tcu-accordion-header').click();
    cy.get(':nth-child(1) > .tcu-accordion-content').should('not.be.visible');
  });

  it('Accordion opens one at a time on click', function () {
    cy.get(':nth-child(2) > .tcu-accordion-header').click();
    cy.get(':nth-child(2) > .tcu-accordion-content').should('be.visible');
    cy.get(':nth-child(1) > .tcu-accordion-content').should('not.be.visible');
  });
});
