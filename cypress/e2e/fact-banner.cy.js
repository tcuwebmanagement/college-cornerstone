/// <reference types="cypress" />

const WRAPPER = 'body';
const delay = 300;

context('Fact Banner', () => {
  beforeEach(() => {
    cy.visit('/components/preview/fact-banner');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  it('Next slider arrow advances to the next slide', function () {
    cy.viewport('iphone-6+');

    cy.wait(delay);

    // Starts on first slide
    cy.get('.flickity-slider .tcu-fact-banner__card')
      .eq(0)
      .should('have.class', 'is-selected');
    // Click on next arrow
    cy.get('.next > .flickity-button-icon').click();
    // First slide should no longer be active, next slide should be active
    cy.wait(delay)
      .get('.flickity-slider .tcu-fact-banner__card')
      .eq(0)
      .should('not.have.class', 'is-selected');
    cy.get('.flickity-slider .tcu-fact-banner__card')
      .eq(1)
      .should('have.class', 'is-selected');
  });

  it('Previous slider arrow reverses to the previous slide', function () {
    cy.viewport('iphone-6+');

    cy.wait(delay);

    // Starts on first slide
    cy.get('.flickity-slider .tcu-fact-banner__card')
      .eq(0)
      .should('have.class', 'is-selected');
    // Click on previous arrow
    cy.get('.previous > .flickity-button-icon').click();
    // First slide should no longer be active, last slide should be active
    cy.get('.flickity-slider .tcu-fact-banner__card')
      .eq(0)
      .should('not.have.class', 'is-selected');
    cy.wait(delay)
      .get('.flickity-slider .tcu-fact-banner__card')
      .eq(-1)
      .should('have.class', 'is-selected');
  });

  it('Slider dots navigate to the same slide', function () {
    cy.viewport('iphone-6+');

    cy.wait(delay);

    cy.get('.flickity-page-dots .dot').each(($el, index, $list) => {
      // Click on the index dot
      cy.wrap($el).click();
      // Previous slide should no longer be active, index slide should be active
      cy.wait(delay)
        .get('.flickity-slider .tcu-fact-banner__card')
        .eq(index - 1)
        .should('not.have.class', 'is-selected');
      cy.get('.flickity-slider .tcu-fact-banner__card')
        .eq(index)
        .should('have.class', 'is-selected');
    });
  });
});
