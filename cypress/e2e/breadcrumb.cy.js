/// <reference types="cypress" />

const WRAPPER = 'nav';

context('Breadcrumb', () => {
  beforeEach(() => {
    cy.visit('/components/preview/breadcrumb');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  // Nav bar exists
  it('Breadcrumb exists', () => {
    cy.get('nav').should('exist');
  });

  // Nav bar has children
  it('Breadcrumb has children', () => {
    cy.get('nav li').should('not.have.length', 0);
  });
});
