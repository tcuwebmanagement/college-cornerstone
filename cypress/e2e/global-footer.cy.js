/// <reference types="cypress" />

const WRAPPER = '.tcu-footer-nav-wrap';

context('Global Footer', () => {
  beforeEach(() => {
    cy.visit('/components/preview/global-footer');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
