/// <reference types="cypress" />

const WRAPPER = '.tcu-publications-feed';

context('Title Card', () => {
  beforeEach(() => {
    cy.visit('/components/preview/publications-feed');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
