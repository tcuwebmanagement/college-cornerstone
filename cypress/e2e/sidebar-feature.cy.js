/// <reference types="cypress" />

const WRAPPER = '.tcu-image-card--sidebar';

context('Sidebar Feature', () => {
  beforeEach(() => {
    cy.visit('/components/preview/sidebar-feature--grey');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
