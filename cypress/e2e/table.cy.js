/// <reference types="cypress" />

const WRAPPER = 'table';

context('Tables', () => {
  describe('Default Tables', () => {
    beforeEach(() => {
      cy.visit('/components/preview/tables--default');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER);
    });
  });
  describe('Responsive Tables', () => {
    beforeEach(() => {
      cy.visit('/components/preview/responsive-table-container');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on mobile', () => {
      cy.viewport('iphone-6+');
      cy.checkAccessibility(WRAPPER);
    });
  });
  describe('Scrollable Tables', () => {
    beforeEach(() => {
      cy.visit('/components/preview/tables--scrollable');
      cy.injectAxe();
    });

    it('Has no detectable a11y violations on desktop', () => {
      cy.checkAccessibility(WRAPPER);
    });

    it('Has no detectable a11y violations on mobile', () => {
      cy.viewport('iphone-6+');
      cy.checkAccessibility(WRAPPER);
    });
  });
});
