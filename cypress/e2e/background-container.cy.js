/// <reference types="cypress" />

const WRAPPER = '.tcu-background-container';

context('Background Container', () => {
  beforeEach(() => {
    cy.visit('/components/preview/background-container--default');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
