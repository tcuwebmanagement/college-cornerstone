/// <reference types="cypress" />

const WRAPPER = '.tcu-flags';
const delay = 300;

context('Sticky Flags', () => {
  beforeEach(() => {
    cy.visit('/components/preview/homepage-with-flags');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  it('Hides words and displays icons according to the window scroll', function () {
    cy.get('.tcu-flags').should('have.class', '_onleave');
    cy.scrollTo('bottom');
    cy.wait(delay);
    cy.get('.tcu-flags').should('have.class', '_onenter');
    cy.scrollTo('top');
    cy.wait(delay);
    cy.get('.tcu-flags').should('have.class', '_onleave');
  });
});
