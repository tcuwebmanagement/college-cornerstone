/// <reference types="cypress" />

const WRAPPER = '.tcu-hero';

context('Hero', () => {
  beforeEach(() => {
    cy.visit('/components/preview/hero--default');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
