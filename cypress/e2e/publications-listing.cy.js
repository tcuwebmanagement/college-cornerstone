/// <reference types="cypress" />

const WRAPPER = '.tcu-publications-listing';

context('Title Card', () => {
  beforeEach(() => {
    cy.visit('/components/preview/publications-listing');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  it('Publication Listing can be toggled on click', function () {
    cy.get(
      '.tcu-publications-listing__item:first-child .tcu-publications-author__toggle',
    ).click();
    cy.get(
      '.tcu-publications-listing__item:first-child .tcu-publications-listing__item-list',
    ).should('be.visible');
    cy.get(
      '.tcu-publications-listing__item:first-child .tcu-publications-author__toggle',
    ).click();
    cy.get(
      '.tcu-publications-listing__item:first-child .tcu-publications-listing__item-list',
    ).should('not.be.visible');
  });
});
