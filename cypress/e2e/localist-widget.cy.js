/// <reference types="cypress" />

const WRAPPER = '.localist-widget-wrapper';

context('Localist Widget', () => {
  beforeEach(() => {
    if (Cypress.browser.family === 'chromium') {
      Cypress.automation('remote:debugger:protocol', {
        command: 'Network.enable',
        params: {},
      });
      Cypress.automation('remote:debugger:protocol', {
        command: 'Network.setCacheDisabled',
        params: { cacheDisabled: true },
      });
    }
    cy.intercept('https://calendar.tcu.edu/widget/view**').as('getCalendar');
    cy.visit('/components/preview/localist-widget');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.wait('@getCalendar').then((interceptions) => {
      cy.get('a, button').first().focus();
      cy.checkAccessibility(WRAPPER);
    });
  });
});
