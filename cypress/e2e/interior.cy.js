/// <reference types="cypress" />

const WRAPPER = 'body';

context('Interior Page', () => {
  beforeEach(() => {
    cy.visit('/components/preview/interior');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER, {
      // We need to disable the following rules because we purposely have two banners
      // One for the local site and one for the global TCU sites
      rules: {
        region: { enabled: false },
        'landmark-no-duplicate-banner': { enabled: false },
        'landmark-unique': { enabled: false },
      },
    });
  });
});
