/// <reference types="cypress" />

const WRAPPER = 'body';

context('Button Group', () => {
  beforeEach(() => {
    cy.visit('/components/preview/button-group');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
