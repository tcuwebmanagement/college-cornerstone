/// <reference types="cypress" />

const WRAPPER = 'body';
const delay = 300;

context('Global Navigation', () => {
  beforeEach(() => {
    cy.visit('/components/preview/global-nav');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  function openNavigation() {
    // Navigation is initially closed
    cy.get('.tcu-content-sheet').should('not.be.visible');
    // Click toggle button
    cy.get('.tcu-menu-toggle').click();
    // Navigation should now be open
    cy.get('.tcu-content-sheet').should('be.visible');
  }

  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport('macbook-13');
    });

    it('Navigation opens', () => {
      openNavigation();
    });

    it('Navigation closes', () => {
      openNavigation();
      // Click the close button
      cy.get('.tcu-close-contentsheet').click();
      // Navigation should now be closed
      cy.get('.tcu-content-sheet').should('not.be.visible');
    });
  });

  context('Mobile', () => {
    beforeEach(() => {
      cy.viewport('iphone-6+');
    });

    it('Navigation opens', () => {
      // Navigation is initially closed
      cy.get('.tcu-content-sheet').should('not.be.visible');
      // Click toggle button
      cy.get('.tcu-menu-toggle').click();
      // Navigation should now be open
      cy.get('.tcu-content-sheet').should('be.visible');
    });

    it('Navigation closes', () => {
      // Navigation is initially closed
      cy.get('.tcu-content-sheet').should('not.be.visible');
      // Click toggle button
      cy.get('.tcu-menu-toggle').click();
      // Navigation should now be open
      cy.get('.tcu-content-sheet').should('be.visible');
      // Click the close button
      cy.get('.tcu-close-contentsheet').click();
      // Navigation should now be closed
      cy.get('.tcu-content-sheet').should('not.be.visible');
    });
  });
});
