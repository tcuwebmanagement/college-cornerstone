/// <reference types="cypress" />

const WRAPPER = '.tcu-hero-text';

context('Site Name', () => {
  beforeEach(() => {
    cy.visit('/components/preview/site-name--default');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});

context('Site Name with Department', () => {
  beforeEach(() => {
    cy.visit('/components/preview/site-name--department');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
