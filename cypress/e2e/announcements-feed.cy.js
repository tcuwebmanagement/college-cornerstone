/// <reference types="cypress" />

const WRAPPER = '.tcu-announcements';

context('Announcements Feed', () => {
  beforeEach(() => {
    cy.visit('/components/preview/announcements-feed');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
