/// <reference types="cypress" />

const WRAPPER = '.tcu-content-button';

context('Content Button', () => {
  beforeEach(() => {
    cy.visit('/components/preview/content-button');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
