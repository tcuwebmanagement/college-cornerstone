/// <reference types="cypress" />

const WRAPPER = '.tcu-infographic-ribbon';

context('Infographic Ribbon', () => {
  beforeEach(() => {
    cy.visit('/components/preview/infographic-ribbon');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
