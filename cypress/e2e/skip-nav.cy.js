/// <reference types="cypress" />

const WRAPPER = '.tcu-skip-nav';

context('Hero', () => {
  beforeEach(() => {
    cy.visit('/components/preview/skip-nav');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  it('Has the show on focus class', () => {
    cy.get('.tcu-skip-nav').should('have.class', 'show-on-focus');
  });

  it('Is visible on focus', () => {
    cy.get('.tcu-skip-nav').focus();
    cy.get('.tcu-skip-nav').should('be.visible');
  });
});
