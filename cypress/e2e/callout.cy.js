/// <reference types="cypress" />

const WRAPPER = 'body';

context('Callout - Dark', () => {
  beforeEach(() => {
    cy.visit('/components/preview/callout--default');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});

context('Callout - Light', () => {
  beforeEach(() => {
    cy.visit('/components/preview/callout--light');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
