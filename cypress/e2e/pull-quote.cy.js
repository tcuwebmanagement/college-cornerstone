/// <reference types="cypress" />

const WRAPPER = '.tcu-pull-quote';

context('Pull Quote', () => {
  beforeEach(() => {
    cy.visit('/components/preview/pull-quote--default');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
