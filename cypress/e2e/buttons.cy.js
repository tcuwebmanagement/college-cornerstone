/// <reference types="cypress" />

const WRAPPER = 'body';

context('Buttons', () => {
  beforeEach(() => {
    cy.visit('/components/preview/buttons');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
