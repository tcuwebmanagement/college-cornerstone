/// <reference types="cypress" />

const WRAPPER = '.tcu-top';
const delay = 300;

context('Back To Top Button', () => {
  beforeEach(() => {
    cy.visit('/components/preview/interior');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });

  it('Back to Top Button scrolls to top', function () {
    cy.scrollTo('bottom').get('.tcu-top').click();
    // Check scroll position
    cy.wait(delay * 3)
      .window()
      .then(($window) => {
        expect($window.scrollY).to.be.closeTo(0, 100);
      });
  });
});
