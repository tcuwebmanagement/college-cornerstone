/// <reference types="cypress" />

const WRAPPER = '.tcu-title-card';
const delay = 300;

context('Title Card with Large Buttons', () => {
  beforeEach(() => {
    cy.intercept('https://player.vimeo.com/api/player.js', (req) => {
      req.on('before:response', (res) => {
        res.headers['cache-control'] = 'no-store';
      });
    }).as('vimeo');
    cy.visit('/components/preview/title-card--large-buttons');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.wait('@vimeo').then(() => {
      // This is to help make sure vimeo has been initialized before running checkA11y
      cy.get(WRAPPER).should('have.descendants', 'iframe');
      cy.checkAccessibility(WRAPPER);
    });
  });

  it('Video button switches to pause', () => {
    cy.viewport('macbook-15');
    cy.wait('@vimeo').then(() => {
      cy.get('._pause').should('contain.text', 'Pause');
      cy.get('._pause').click();
      cy.get('._pause').should('contain.text', 'Play');
    });
  });

  it('Video button switches to play', () => {
    cy.viewport('macbook-15');
    cy.wait('@vimeo').then(() => {
      cy.get('._pause').should('contain.text', 'Pause');
      cy.get('._pause').click();
      cy.get('._pause').should('contain.text', 'Play');
      cy.get('._pause').click();
      cy.get('._pause').should('contain.text', 'Pause');
    });
  });
});
