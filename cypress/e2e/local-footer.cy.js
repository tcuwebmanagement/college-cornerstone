/// <reference types="cypress" />

const WRAPPER = '.tcu-local-footer';

context('Local Footer', () => {
  beforeEach(() => {
    cy.visit('/components/preview/local-footer');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
