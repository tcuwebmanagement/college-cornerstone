/// <reference types="cypress" />

const WRAPPER = '.tcu-majors-programs';

context('Majors and Programs', () => {
  beforeEach(() => {
    cy.visit('/components/preview/majors-and-programs');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
