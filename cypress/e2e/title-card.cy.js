/// <reference types="cypress" />

const WRAPPER = '.tcu-title-card';

context('Title Card', () => {
  beforeEach(() => {
    cy.visit('/components/preview/title-card--default');
    cy.injectAxe();
  });

  it('Has no detectable a11y violations on load', () => {
    cy.checkAccessibility(WRAPPER);
  });
});
