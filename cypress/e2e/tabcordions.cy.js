/// <reference types="cypress" />

const WRAPPER = '.tcu-tabcordion';
const delay = 600;

context('Tabcordions', () => {
  beforeEach(() => {
    cy.intercept('/js/tcu-tabcordions.min.js').as('tabcordions-js');
    cy.visit('/components/preview/tabcordions');
    cy.injectAxe();
    cy.wait('@tabcordions-js');
  });

  context('Mobile', () => {
    beforeEach(() => {
      cy.viewport('iphone-6+');
      cy.wait(delay);
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER);
    });

    it('Opens a new tab', () => {
      cy.get('#tab-label3').click();
      // Panel 1 closes
      cy.get('#tab-label1').should('not.have.attr', 'aria-selected', 'true');
      cy.get('#tab-panel1').should('not.be.visible');
      // Panel 3 opens
      cy.get('#tab-panel3').should('be.visible');
      cy.get('#tab-label3').should('have.attr', 'aria-selected', 'true');
    });
  });

  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport('macbook-13');
      cy.wait(delay);
    });

    it('Has no detectable a11y violations on load', () => {
      cy.checkAccessibility(WRAPPER, {
        rules: {
          region: { enabled: false },
        },
      });
    });

    it('Opens a new tab', () => {
      cy.get('#tab-label3').click();
      // Panel 1 closes
      cy.get('#tab-label1').should('not.have.attr', 'aria-selected', 'true');
      cy.get('#tab-panel1').should('not.be.visible');
      // Panel 3 opens
      cy.get('#tab-panel3').should('be.visible');
      cy.get('#tab-label3').should('have.attr', 'aria-selected', 'true');
    });
  });
});
