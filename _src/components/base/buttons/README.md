Buttons can be `a` or `button` elements. They can be modified with various classes.

**If you are linking to a page or an anchor, use the `a` element.**

Example:

```
<a class="tcu-button _primary" href="https://www.tcu.edu/apply.php">Apply</a>
```

**If you are triggering an event with JavaScript, use the `button` element.**

Example:

```
<button class="tcu-button _secondary">Expand Menu<button>
```

## How to style a component button

Instead of using specific button classes like secondary or hollow in a component,
style the button using button mixins and css custom properties. This allows us to
be more flexible with button styles within components.

CSS Custom Properties:

- --button-background
- --button-color
- --button-border-color
- --button-border-width

Mixins:

- primary-button
- hover-button
- secondary-button
- hollow-button
- clear-button

Example of a component with hollow buttons set with mixins:

```
<div class="tcu-component">
  ...
  <div class="button-group">
    <a href="#" class="button">Visit</a>
  </div>
</div>
```

```
  .tcu-component {
    @include hollow-button();
  }
```
