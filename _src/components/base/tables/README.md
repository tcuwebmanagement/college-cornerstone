# Default

For the Default table, the following classes are available to customize the table:

- unstriped
- hover
- stack
- scroll

# Responsive

A responsive table reorders itself on mobile so that it's easier to view. This is best for simple tables.

# Scrollable

For complex tables, it's best to use a scrollable table. This keeps the information in its original format on mobile and uses a scrolling container.

A caption is required for accessibility but doesn't need to render on the front end.
