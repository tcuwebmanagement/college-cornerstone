/* eslint-disable no-undef */
const axios = require('axios');
const { XMLParser } = require('fast-xml-parser');

// make the request to the API, returns a Promise
const response = axios
  .get('https://www.tcu.edu/rss/news.xml')
  .then(function (response) {
    const parser = new XMLParser({
      ignoreAttributes: false,
      attributeNamePrefix: '@_',
    });
    const jsonObj = parser.parse(response.data, { ignoreAttributes: false });
    let items = jsonObj.rss.channel.item.slice(0, 6);
    const articles = [];

    items.forEach((element, index) => {
      articles.push({
        id: index,
        image: element['media:content']
          ? element['media:content']['media:thumbnail']['@_url']
          : '',
        image_description: element['media:content']
          ? element['media:content']['media:description']
          : '',
        tags: element.category,
        permalink: element.link,
        title: element.title,
        date: 'September 16, 2020',
        description: `<p>${element.description}</p>`,
      });
    });

    return articles;
  });

module.exports = {
  tags: ['ou-system'],
  status: 'complete',
  context: {
    modifier: '_card',
    limit: 6,
    articles: response,
  },
  variants: [
    {
      name: 'default',
      label: 'Card Style',
    },
    {
      name: 'blog',
      label: 'Blog Style',
      context: {
        modifier: '_blog',
        limit: 3,
      },
    },
  ],
};
