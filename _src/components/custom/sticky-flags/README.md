If the user is on the page that a sticky flag links to, that flag should be hidden. See https://admissions.tcu.edu/index.php as an example.

The sticky flags require some JS to show/hide the flags on scroll. Whenever a user has scrolled down into the main section of the site, they minimize to an icon only view.

Svgs and background colors are inlined in the the sticky-flags.php (e.g. in the hr site, site/\_resources/includes/sticky-flags.php)

Flag background color is set to $neutral-40, and icon background color is set to $primary by default.

CSS variable for flags' background: --sticky-bg-color.

CSS variable for icons' background: --sticky-icon-bg-color.
