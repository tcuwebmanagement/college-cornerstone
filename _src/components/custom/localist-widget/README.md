This localist widget should be created as an asset.

For this style of localist widget, the following parameters need to be set:

- `hideimage=1`
- `hidedesc=1`
- `show_times=1`
- `style=none`
