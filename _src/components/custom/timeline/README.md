<strong>Timeline Component</strong>

The Timeline component is designed to elegantly showcase significant historical events across various topics. It offers two distinct versions:

<strong>Default Timeline:</strong> Perfect for larger displays, this version emphasizes detailed presentation and visual impact.

<strong>Basic Timeline:</strong> A scaled-down alternative suitable for smaller projects, offering essential features while maintaining clarity and simplicity.

Ideal for educational purposes, storytelling, or any application where chronological data visualization is essential, the Timeline component provides flexibility and aesthetic appeal.
