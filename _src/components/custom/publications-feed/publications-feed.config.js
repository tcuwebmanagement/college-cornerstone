/* eslint-disable no-undef */
const axios = require('axios');
const { XMLParser } = require('fast-xml-parser');

// make the request to the API, returns a Promise
const response = axios
  .get(
    'https://www.tcu.edu/directory/list/addran/criminal-justice.publications.xml',
  )
  .then(function (response) {
    const parser = new XMLParser({
      ignoreAttributes: false,
      attributeNamePrefix: '@_',
    });
    const jsonObj = parser.parse(response.data);
    let authors = jsonObj.authors.author;
    const publications = [];

    authors.forEach((author) => {
      if (author.publications) {
        author.publications.publication.forEach((item) => {
          publications.push({
            title: item['@_title'],
            author: author['@_name'],
            date: item['@_publish-date'],
            permalink: item.permalink,
          });
        });
      }
    });

    publications.sort(function (a, b) {
      return new Date(b.date) - new Date(a.date);
    });

    return publications.slice(0, 4);
  });

module.exports = {
  tags: ['ou-system'],
  status: 'complete',
  context: {
    publications: response,
  },
};
