## TCU Footer

The footer module used on all TCU academic/non-academic websites. The footer contains the logo/mark for the site specific school/college or department, the social stream for the college, the college location on campus, contact information, quick links, call-to-action and social media links.
