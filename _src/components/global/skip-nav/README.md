Links to #main which is positioned at the very top of the main content area

```
<a tabindex="-1" id="main"><span class="show-for-sr">Main Content</span></a>
```
