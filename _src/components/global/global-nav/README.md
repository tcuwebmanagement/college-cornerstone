A navigation bar used for displaying the TCU brand and academic/non-academic site menu items. Global navigation is used on every academic/non-academic TCU website.
