The profile card set is meant to be used in combination with the profile cards. It's a grid to contain a larger number of profile cards.

- On small screens, the set displays two cards per row
- On medium screens, the set displays three cards per row
- On large screens, the set displays four cards per row
