/* eslint-disable no-undef */
const axios = require('axios');

// make the request to the API, returns a Promise
const response = axios
  .get('https://www.tcu.edu/directory/list/addran/index.json')
  .then(function (response) {
    const profiles = [];
    for (let index = 0; index < response.data.fsprofiles.length; index++) {
      const element = response.data.fsprofiles[index];
      if (!element.image) {
        element.image = '/directory/images/leadon-default-headshot-mobile.jpg';
      }
      element.image = 'https://www.tcu.edu' + element.image;
      profiles.push(element);
      element.description = `${element.telephone}<br><a href="mailto:${element.email}">${element.email}</a><br><a href="#" aria-label="View ${element.name} Profile">View Profile</a>`;
    }
    return profiles;
  });

module.exports = {
  tags: ['ou-snippet', 'wordpress-block'],
  status: 'complete',
  context: {
    directory: response, // use the response as context data for our template.
  },
};
