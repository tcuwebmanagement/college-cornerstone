### Background Container

The background container...

The use case is currently limited to WWW(tcu.edu), CSE, Admissions, Addran.

```html
<div
  id="tcu-background-container-5"
  class="tcu-module-wrapper _full-width tcu-background-container _has-image "
  style="background-color: #fff"
>
  <div class="tcu-background-container__image"></div>
</div>
```
