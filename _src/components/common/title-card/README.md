**Suggested Image Sizes for Image Left/Right**:

- Small - 320x200
- Medium - 750x400
- Large - 750x600

## Background Colors:

Works with Image Left/Right and Basic

- None - `_white`
- Grey - `_grey`
- Purple - `_purple`

## Flexbox Width (optional):

Must be inside a module-wrapper

```html
<div class="tcu-module-wrapper">...</div>
```

- Default - `_flex-1of1`
- 2/3 - `_flex-2of3`
- 1/2 - `_flex-1of2`
- 1/3 - `_flex-1of3`
- 1/4 - `_flex-1of4`
