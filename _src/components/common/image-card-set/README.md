An Image Card Set can have 1 to 4 image cards grouped together.

When there are 3 or less image cards in a set, they display in a single row.

When there are 4 image cards, they are split into a 2x2 grid.
