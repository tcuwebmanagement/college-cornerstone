Create a show/hide accordion-style element

The accordion is set to automatically open the first accordion in the accordion container.

For <strong>omniCMS</strong>, to have a <strong>closed</strong> accordion by default, make sure you add the class <strong>`_collapsed`</strong> to the accordion table.

For <strong>development</strong>, to have a <strong>closed</strong> accordion by default, make sure you add <strong>`data-open="false"`</strong> in the accordion header.
