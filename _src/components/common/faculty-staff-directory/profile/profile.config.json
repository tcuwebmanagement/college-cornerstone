{
  "meta": {
    "scripts": ["/js/tcu-accordions.min.js", "/js/scripts.min.js"]
  },
  "context": {
    "name": "Dave Aftandilian",
    "id": "3f65be58-6c36-4195-a0c7-65c52a365360",
    "filename": "dave-aftandilian",
    "source": "https://www.tcu.edu/directory/_data/dave-aftandilian.json",
    "canonical": "/directory/_data/dave-aftandilian.json",
    "media-approved": "",
    "publication-approved": "true",
    "updated": "2021-03-11T15:02:01.667Z",
    "familyName": "Aftandilian",
    "givenName": "Dave",
    "honorificSuffix": "Ph.D",
    "jobTitle": "Associate Professor of Anthropology and Director of Human-Animal Relationships (HARE) minor",
    "pronouns": "he/him/his",
    "image": "/directory/images/Dave-aftandilian1-profile-lg.jpg",
    "email": "d.aftandilian@tcu.edu",
    "telephone": "817-257-4540",
    "office": "Scharbauer Hall 4221",
    "mapLink": "",
    "tcuBox": "",
    "curriculumVitae": "test",
    "areasOfInterest": ["Environmental Studies", "Animal Studies"],
    "roles": [
      {
        "type": "faculty",
        "title": "Associate Professor",
        "college": {
          "id": "addran",
          "displayName": "AddRan College of Liberal Arts",
          "url": "https://addran.tcu.edu"
        },
        "departments": [
          {
            "id": "sociology--anthropology",
            "displayName": "Sociology & Anthropology",
            "url": "https://addran.tcu.edu/sociology-anthropology/"
          }
        ],
        "teaching-status": { "id": "active", "displayName": "Active" },
        "teaching-audiences": [
          {
            "id": "undergraduate-students",
            "displayName": "Undergraduate Students"
          }
        ],
        "areas-study": [
          {
            "id": "human-animal-relationships",
            "displayName": "Human-Animal Relationships"
          }
        ]
      }
    ],
    "biography": {
      "wysiwyg-text": "<h2 class=\"h3\">Education</h2><p><span>Ph.D. in Anthropology, University of Chicago</span><br/><span>MA in Anthropology, University of Chicago</span><br/><span>BA in Classics and College Scholar, Cornell University</span></p><h2 class=\"h3\">Courses Taught</h2><p><span>ANTH 20633: Introduction to Archaeology</span><br/><span>ANTH 30663: Food Justice</span><br/><span>ANTH 30743: Animals, Religion &amp; Culture</span><br/><span>ANTH 30783: Anth. Approaches to Nature &amp; the Sacred</span><br/><span>ANTH 30823: Native American Religions &amp; Ecology<br/></span><span>ANTH 30923: Into the Small--Little Animals in Art, Culture &amp; Museums</span><br/><span>ANTH 40214: Human Osteology</span></p><h2 class=\"h3\">Areas of Focus</h2><p><span>Animals, Religion &amp; Culture; Human-Animal Studies; Food Justice; Native American Studies; Illinois Archaeology; Sustainability; Community Engagement; Contemplative Studies; Acoustic Ecology</span></p><div class=\"tcu-accordion-container _collapsed \"><button type=\"button\" class=\"tcu-accordion-header\" id=\"selected-publications\" data-open=\"false\">Selected Publications</button><div class=\"tcu-accordion-content\"><p>Coeditor, <span><em>Grounding Education in the Environmental Humanities: Exploring Place-Based Pedagogies in the South</em> (Routledge, 2019)</span></p><p>Coeditor,<span> </span><em>City Creatures: Animal Encounters in the Chicago Wilderness </em>(Univ. of Chicago Press, 2015)</p><p>Editor,<span> </span><em>What Are the Animals to Us? Approaches from Science, Religion, Folklore, Literature, and Art</em><span> </span>(Univ. of Tennessee Press, 2007)</p><p>Author, “Animals and Religion,” in Whitney A. Bauman, Richard R. Bohannon II, and Kevin J. O’Brien, eds.,<span> </span><em>Grounding Religion: A Field Guide to the Study of Religion and Ecology,</em><span> </span>second ed. (Routledge, 2017)</p><p>Coauthor, “Using Garden-Based Service-Learning to Work Toward Food Justice, Better Educate Students, and Strengthen Campus-Community Ties,”<span> </span><em>Journal of Community Engagement and Scholarship</em><span> </span>6 (2013): 55-69</p><p>Author, “Interpreting Animal Effigies from Precontact Native American Sites: Applying an Interdisciplinary Method to Illinois Mississippian Artifacts,” pp. 62-70 in María Cecilia Lozada and Barra Ó Donnabhain, eds.,<span> </span><em>The Dead Tell Tales: Essays in Honor of Jane E. Buikstra</em><span> </span>(Cotsen Institute of Archaeology Press, 2013)</p><p>Author, “What Other Americans Can and Cannot Learn from Native American Environmental Ethics,”<span> </span><em>Worldviews: Global Religions, Culture, and Ecology</em><span> </span>15 (2011): 219-246</p><p>Author, “Toward a Native American Theology of Animals: Creek and Cherokee Perspectives,”<span> </span><em>CrossCurrents</em><span> </span>61 (2011): 191-207</p><p>Author, “Corn Mother in North America: Life-Bringer and Culture-Bearer,” pp. 135-148 in Patricia Monaghan, ed.,<span> </span><em>Goddesses in World Cultures, Vol. 3: Australia and the Americas</em><span> </span>(Praeger, 2011)</p></div><button type=\"button\" class=\"tcu-accordion-header\" id=\"selected-presentations\">Selected Presentations</button><div class=\"tcu-accordion-content\"><p>“Connecting to Place &amp; Animals through Contemplative Practices,” paper presented at American Academy of Religion annual conference in Atlanta, GA, November 20-24, 2015</p><p>“Animals, Religion, and the City,” invited presentation as part of panel on “City Creatures: Animals, Religion, and Ethics” for the Chicago Theological Seminary, September 29, 2015</p><p>“From Hearing to Caring: The Role of Listening in Place-Based Pedagogy,” paper presented at conference “Ecomusics and Ecomusicologies: Dialogues 2014” in Asheville, NC, October 3-6, 2014</p><p>“How Do Food Banks’ Gardens Grow? Successes and Challenges for Food Bank-Sponsored Community Gardens and Urban Agriculture,” paper presented at “Global Gateways &amp; Local Connections: Cities, Agriculture, and the Future of Food Systems,” biannual meeting of the Agriculture, Food &amp; Human Values Society, Association for the Study of Food and Society, and the Society for the Anthropology of Food and Nutrition, New York, New York, June 20-24, 2012</p><p>“Authentically Assessing Students’ Global and Cultural Awareness,” panel presentation with Dr. Manochehr Dorraj, TCU Political Science Dept., and Ms. Janice Elliott, TCU Neely School of Business, at conference on “General Education and Assessment: New Contexts, New Cultures,” Association of American Colleges and Universities (AACU), New Orleans, Louisiana, February 23-25, 2012</p><p>“Ducks in the Illinois Mississippian Mind: Animals, Agriculture, and Religion among a Precontact Native American People,” American Anthropological Association Annual Meeting, San Francisco, California, November 19-23, 2008</p></div><button type=\"button\" class=\"tcu-accordion-header\" id=\"professional-affiliations\">Professional Affiliations</button><div class=\"tcu-accordion-content\"><p><span>Agriculture, Food &amp; Human Values Society</span><br/><span>American Academy of Religion</span><br/><span>American Society for Acoustic Ecology</span><br/><span>International Society for Anthrozoology</span><br/><span>International Society for the Study of Religion, Nature &amp; Culture</span><br/><span>Society for American Archaeology</span></p></div><button type=\"button\" class=\"tcu-accordion-header\" id=\"professional-service\">Professional Service</button><div class=\"tcu-accordion-content\"> \n<ul><li>American Academy of Religion, Animals and Religion Group: steering committee member, 2015 to 2020; co-chair, 2008 to 2014</li><li>American Society for Acoustic Ecology: board member at large, 2015 to present; Secretary, 2010 to 2014</li><li>Tarrant County Food Policy Council: board member, 2014 to 2020; founder and chair, Working Group on Community Gardens &amp; Urban Agriculture, 2012 to present</li></ul></div></div><!--/.tcu-accordion-container-->"
    }
  }
}
