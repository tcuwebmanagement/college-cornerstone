/* eslint-disable no-undef */
const axios = require('axios');

// make the request to the API, returns a Promise
const response = axios
  .get('https://www.tcu.edu/directory/list/addran/index.json')
  .then(function (response) {
    const profiles = [];
    for (let index = 0; index < response.data.fsprofiles.length; index++) {
      const element = response.data.fsprofiles[index];
      if (!element.image) {
        element.image = 'directory/images/leadon-default-headshot-mobile.jpg';
      }
      profiles.push(element);
    }
    return profiles;
  });

module.exports = {
  tags: ['ou-component'],
  context: {
    directory: response, // use the response as context data for our template.
  },
  variants: [
    {
      name: 'default',
      label: 'Grid',
    },
    {
      name: 'List',
    },
  ],
};
