The table wrapper is used to wrap responsive table markup. When adding responsive table snippet,
you will have to create a table inside the snippet block in the WYSIWYG.

```html
<table class="stacked">
  <!-- table markup here -->
</table>
```
