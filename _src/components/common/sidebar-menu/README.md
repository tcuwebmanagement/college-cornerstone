This component implements a sidebar menu. It serves as a vertical navigation element typically positioned on the right side of the screen.
On smaller screens, the sidebar menu stacks at the bottom to ensure optimal usability and responsiveness.
