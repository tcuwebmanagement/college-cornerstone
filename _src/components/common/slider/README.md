A generic slider for the [Flexbox Row](/components/detail/flexbox-row) snippet.
Refer to the [Flickity Options Documenation](https://flickity.metafizzy.co/options)
for available flickity data options.

Supported classes:

- `.slider-for-small-only`
