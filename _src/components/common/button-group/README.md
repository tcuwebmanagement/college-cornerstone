A button group evenly spaces out a grouping of buttons.

For stacked buttons on all screen sizes, add the `stacked` class to the .tcu-button-group container.

For stacked buttons on small screens only, add the `stacked-for-small` class to the .tcu-button-group container.

For even width buttons with no spacing between them, add the `expanded` class to the .tcu-button-group container.
