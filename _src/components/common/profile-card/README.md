**Suggested Image Size**: 160x140

The data for the profile card can either be pulled in via a reference to the person's Faculty/Staff Profile or via manual content entry.

If the data is fed via Faculty/Staff Profile, the name should be hyperlinked.
