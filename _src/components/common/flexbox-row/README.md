The flexbox row uses the [Foundation XY Grid](https://get.foundation/sites/docs/xy-grid.html) classes.

The flexbox snippet in Omni CMS transforms a table into a flexbox grid of rows and columns. By default, small screens use full width columns and large screens will have evenly divided columns.
