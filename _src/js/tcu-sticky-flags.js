(function () {
  const tcuFlags = document.querySelector('.tcu-flags');
  const navWrapper = document.querySelector('.nav-wrapper');

  if (tcuFlags && navWrapper) {
    let isPastTrigger = false;

    window.addEventListener('scroll', () => {
      const stickyFlagTrigger = navWrapper.getBoundingClientRect().top;
      if (stickyFlagTrigger < 0 && !isPastTrigger) {
        tcuFlags.classList.remove('_onleave');
        tcuFlags.classList.add('_onenter');
        isPastTrigger = true;
      } else if (stickyFlagTrigger >= 0 && isPastTrigger) {
        tcuFlags.classList.remove('_onenter');
        tcuFlags.classList.add('_onleave');
        isPastTrigger = false;
      }
    });
  }
})();
