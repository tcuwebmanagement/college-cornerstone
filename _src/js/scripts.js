/* global $ */
/*eslint wrap-iife: [2, "inside"]*/
(function () {
  const $backToTop = document.querySelector('.tcu-top');
  const carousel = document.querySelector('.tcu-carousel._quick-facts');
  const allTagsBtn = document.querySelector('.tcu-tags-toggle');
  const tcuTagsDropdownMenu = document.querySelector('.tcu-tags-dropdown_menu');
  const reduceMotion = window.matchMedia('(prefers-reduced-motion: reduce)');
  const imageGalleries = document.querySelectorAll('.tcu-image-gallery');

  if (reduceMotion.matches === true) {
    document.documentElement.classList.add('reduced-motion');
  }

  /**
   * Scroll to top button
   */
  // browser window scroll (in pixels) after which the "back to top" link is shown
  const offset = 300;

  // browser window scroll (in pixels) after which the "back to top" link opacity is reduced
  const offsetOpacity = 1200;

  // duration of the top scrolling animation (in ms)
  const scrollTopDuration = 700;
  let ticking = false;

  // Event Listeners
  if ($backToTop) {
    window.addEventListener('scroll', scrollListener);
    $backToTop.addEventListener('click', scrollToTopListener);
  }

  if (allTagsBtn) {
    allTagsBtn.addEventListener('click', toggleCategories);
    document.addEventListener('keydown', logKey);
  }

  /**
   * Initialize any Vimeo video that uses data attributes
   */
  const vimeoVideos = document.querySelectorAll('div[data-vimeo-id]');
  if (vimeoVideos) {
    initVideos();
  }
  function initVideos() {
    for (let index = 0; index < vimeoVideos.length; index++) {
      const element = vimeoVideos[index];
      const button = element.parentNode.querySelector('._pause');
      // eslint-disable-next-line no-undef
      const vimeoPlayer = new Vimeo.Player(element.id);

      vimeoPlayer.getPaused().then(function (paused) {
        if (paused) {
          button.innerHTML = '<span aria-hidden="true">►</span> Play';
        }
      });

      vimeoPlayer.on('play', function () {
        button.innerHTML = '<span aria-hidden="true">❚❚</span> Pause';
      });

      vimeoPlayer.on('pause', function () {
        button.innerHTML = '<span aria-hidden="true">►</span> Play';
      });

      vimeoPlayer.ready().then(function () {
        if (reduceMotion.matches === true || isMobile()) {
          vimeoPlayer.pause().then(() => {
            console.log('paused');
          });
        }
      });

      button.addEventListener('click', function () {
        vimeoPlayer.getPaused().then(function (paused) {
          const method = paused ? 'play' : 'pause';
          vimeoPlayer[method]();
        });
      });
    }
  }

  function isMobile() {
    // Check if it's a mobile device
    if (
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/Blackberry/i) ||
      navigator.userAgent.match(/Windows Phone/i)
    ) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Toggle Frog Blog Categories
   * @Author: nicholastillman
   * @Date: 2019-08-30 11:47:32
   * @Desc: Displays the categories menu for the frog blog
   */
  function toggleCategories(e) {
    if (e.target === allTagsBtn || e.target === allTagsBtn.firstElementChild) {
      tcuTagsDropdownMenu.classList.toggle('active');
      allTagsBtn.classList.toggle('active');
    } else {
      tcuTagsDropdownMenu.classList.remove('active');
      allTagsBtn.classList.remove('active');
    }
  }
  function logKey(e) {
    if (e.isComposing || e.keyCode === 229) {
      return;
    }
    if (e.keyCode === 27 && tcuTagsDropdownMenu.classList.contains('active')) {
      tcuTagsDropdownMenu.classList.remove('active');
      allTagsBtn.classList.remove('active');
      allTagsBtn.focus();
    }
  }

  /**
   * Instantiate accordions
   * See tcu.accordions.js for more options
   */

  if (window.tcuAccordions) {
    window.tcuAccordions();
  }

  /**
   * Instantiate slide menus
   * See tcu-slide-menu.js for more options
   */
  const mainNavigation = document.querySelector('.tcu-main-navigation');
  const menuItems = document.querySelectorAll('.tcu-main-navigation > ul > li');
  const navMinWidth = mainNavigation
    ? window
        .getComputedStyle(mainNavigation)
        .getPropertyValue('--nav-min-width')
    : null;
  var currentSize;
  var slideMenus = [];

  const slideMenuResize = debounce(function () {
    // Let's make sure we're only rebuilding the menu when the breakpoint actually changes
    // If the --nav-min-width css variable is present, use that to determine if
    // the breakpoint has changed
    var newSize = getSize(navMinWidth);
    if (currentSize && currentSize === newSize) {
      return;
    }

    // Destroy old menus before rebuilding
    if (slideMenus.length) {
      slideMenus.forEach((menu) => menu.destroy());
      slideMenus = [];
    }

    // Rebuild the menu
    if (newSize === 'large') {
      for (let index = 0; index < menuItems.length; index++) {
        const element = menuItems[index];
        // Only initialize a tcuSlideMenu for menu items with a submenu
        if (element.classList.contains('has-dropdown')) {
          slideMenus.push(
            // eslint-disable-next-line no-undef
            new tcuSlideMenu({
              windowElement: element.querySelector('.sub-menu-wrapper'),
              triggerElement: element.querySelector('button'),
            }),
          );
        }
      }
    } else {
      slideMenus.push(
        // eslint-disable-next-line no-undef
        new tcuSlideMenu({
          addTriggerElementLabel: false,
        }),
      );
    }

    // Update the current size
    currentSize = getSize(navMinWidth);
  }, 250);

  if (menuItems.length > 0) {
    slideMenuResize();
    window.addEventListener('resize', slideMenuResize);
  }

  /* ------- Function Declarations ---------*/

  /**
   * For checking if the screen size is small or large
   * @param {number} [size=1100] large size to test against
   * @returns {string} small or large
   */
  function getSize(size) {
    size = size || 1100;
    let currentWidth =
      window.innerWidth || document.documentElement.clientWidth;
    return currentWidth >= size ? 'large' : 'small';
  }

  /**
   * This function is executed during the scrolling event.
   */
  function scrollListener() {
    let currentPosition =
      window.pageYOffset || document.documentElement.scrollTop;
    if (!ticking) {
      window.requestAnimationFrame(() => {
        swapClass(currentPosition, $backToTop);
        ticking = false;
      });
    }
    ticking = true;
  }

  /**
   * This function is executed on our click event listener for the
   * scroll to top button
   */
  function scrollToTopListener() {
    // Give it a tabindex=-1 to remove the focus for screen readers.
    $backToTop.setAttribute('tabindex', -1);

    // $mobileToggle.setAttribute( 'tabindex', -1 );

    // Check if jQuery is added
    $('html, body').animate({ scrollTop: 0 }, scrollTopDuration);
  }

  /**
   * Swap classes during scroll event.
   *
   * @param {Int}  scrollPos   The position within the window
   * @param {HTML} element     The node element
   */
  function swapClass(scrollPos, element) {
    if (scrollPos > offset) {
      element.classList.add('tcu-is-visible');
    } else {
      element.classList.remove('tcu-is-visible');
      element.classList.remove('tcu-fade-out');
      element.setAttribute('tabindex', 0);
    }
    if (scrollPos > offsetOpacity) {
      element.classList.add('tcu-fade-out');
    }
  }

  /**
   * Adds a caption to the Flickity Image Gallery at the bottom,
   * will update when the image is changed (swapped),
   * Checks for the number of images and adds an extended class if 4 or more
   */
  function handleGalleryImage() {
    const imageGalleryCells =
      this.element.querySelectorAll('.tcu-gallery-cell');
    if (imageGalleryCells.length >= 4) {
      this.element.classList.add('_extended');
    }
    const imageCell = this.selectedElement;
    const imageCaption = this.element.parentNode.querySelector(
      '.gallery-image-caption',
    );
    imageCaption.textContent =
      imageCell && imageCell.querySelector('figcaption')
        ? imageCell.querySelector('figcaption').textContent
        : '';
  }
  /**
   * Returns a function, that, as long as it continues to be invoked, will not
   * be triggered. The function will be called after it stops being called for
   * N milliseconds. If `immediate` is passed, trigger the function on the
   * leading edge, instead of the trailing.
   * @param  {function} func
   * @param  {int} wait
   * @param  {boolean} immediate
   */
  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
        args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      // eslint-disable-next-line no-undef
      clearTimeout(timeout);
      // eslint-disable-next-line no-undef
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  /**
   * For the quick facts carousel
   */

  if (carousel) {
    /* globals Flickity */
    new Flickity(carousel, {
      autoPlay: false,
      adaptiveHeight: true,
      prevNextButtons: true,
      wrapAround: true,
      cellSelector: '.tcu-carousel__cell',
      pageDots: false,
    });
  }

  /**
   * For Image Gallery
   */

  if (imageGalleries) {
    imageGalleries.forEach((imageGallery) => {
      new Flickity(imageGallery, {
        on: {
          ready: handleGalleryImage,
          change: handleGalleryImage,
          resize: handleGalleryImage,
        },
        autoPlay: false,
        adaptiveHeight: false,
        prevNextButtons: true,
        wrapAround: true,
        cellSelector: '.tcu-gallery-cell',
        pageDots: true,
        arrowShape: 'M 10,50 L 60,100 L 70,90 L 30,50 L 70,10 L 60,0 Z',
        imagesLoaded: true,
      });
    });
  }
})();
