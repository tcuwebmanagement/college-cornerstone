/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
(function () {
  const videos = document.querySelectorAll('.tcu-video-showcase');

  for (let index = 0; index < videos.length; index++) {
    init(videos[index]);
  }

  function init(element) {
    const poster = element.querySelector('.tcu-video-showcase__poster-btn');

    poster.addEventListener('click', loadVideo);
  }

  function loadVideo(event) {
    const video = event.target.parentNode.parentNode.querySelector(
      '.tcu-video-showcase__video-src',
    );

    updateVideoSrc(video);
    event.target.parentNode.parentNode.classList.add('_is-active');
  }

  /**
   * Updates the video src
   * @param {HTML} targetVideo video element
   */
  function updateVideoSrc(targetVideo) {
    const videoSrc = targetVideo.getAttribute('data-src');

    if (targetVideo.getAttribute('src') === null) {
      targetVideo.src = videoSrc;

      if (targetVideo.nodeName === 'SOURCE') {
        targetVideo.parentNode.load();
        targetVideo.parentNode.play();
      }
    }
  }
})();
