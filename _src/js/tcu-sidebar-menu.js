(function () {
  const sidebarMenuButtons = document.querySelectorAll(
    '.tcu-sidebar-menu__arrow',
  );
  const sidebarMenuTitle = document.querySelector(
    '.tcu-sidebar-menu__title-wrapper',
  );
  const sidebarTitleButton =
    sidebarMenuTitle &&
    sidebarMenuTitle.querySelector('.tcu-sidebar-menu__arrow');

  const initialWindowWidth =
    window.innerWidth || document.documentElement.clientWidth;

  const sidebarSubmenus = document.querySelectorAll('.tcu-sidebar__submenu');

  // Ensure sidebar menu is closed on small screens (below 1024px) on page load.
  if (initialWindowWidth < 1024 && sidebarTitleButton) {
    sideBarMenuTitleSelected(sidebarTitleButton);
  }

  /**
   * Checks each element with the class "tcu-sidebar__submenu on load".
   * If the element does not contain the class "current-page-item", or "current-page-parent"
   * it calls the function "sideBarMenuSelected" to close submenus.
   * The element's child button that contains the class "tcu-sidebar-menu__arrow".
   */
  if (sidebarSubmenus) {
    sidebarSubmenus.forEach((submenu) => {
      // Check if the submenu contains the class "current-page-item" or "current-page-parent"
      if (
        !(
          submenu.classList.contains('current-page-item') ||
          submenu.classList.contains('current-page-parent')
        )
      ) {
        // Find the child button with the class "tcu-sidebar-menu__arrow"
        const submenuButton = submenu.querySelector('.tcu-sidebar-menu__arrow');

        // If the target button exists, call the sideBarMenuSelected function
        if (submenuButton) {
          sideBarMenuSelected(submenuButton);
        }
      }
    });
  }
  /**
   * Loop through all buttons to find the clicked one and set it as "selectedButton".
   * If the selected button is inside a submenu, call sideBarMenuSelected().
   * If it is in the title, call sideBarMenuTitleSelected().
   */
  if (sidebarMenuButtons) {
    sidebarMenuButtons.forEach((button) => {
      button.addEventListener('click', function () {
        const selectedButton = this;
        const buttonParent = selectedButton.parentNode;
        if (buttonParent.classList.contains('tcu-sidebar__submenu')) {
          sideBarMenuSelected(selectedButton);
        } else if (
          buttonParent.classList.contains('tcu-sidebar-menu__title-wrapper')
        ) {
          sideBarMenuTitleSelected(selectedButton);
        }
      });
    });
  }

  // Ensure the sidebar menu is expanded on large screens (beyond 1024px).
  window.addEventListener('resize', () => {
    const windowWidth =
      window.innerWidth || document.documentElement.clientWidth;
    if (sidebarTitleButton) {
      const isExpanded =
        sidebarTitleButton.getAttribute('aria-expanded') === 'true';
      if (windowWidth > 1024 && !isExpanded) {
        sideBarMenuTitleSelected(sidebarTitleButton);
      }
    }
  });

  /**
   * Toggle the expansion state of the whole sidebar menu.
   * @param {HTMLElement} target - The button element representing the sidebar menu.
   */

  function sideBarMenuTitleSelected(target) {
    const titleButton = target;
    const menuWrapper = titleButton.parentNode.parentNode.querySelector(
      '.tcu-sidebar-menu__wrapper',
    );
    const buttonDescription = target.querySelector('span');

    // Boolean indicating whether the menu is expanded or not.
    const isExpanded = target.getAttribute('aria-expanded') === 'true';

    // Toggle the aria-expanded and aria-hidden attributes.
    toggleAriaAttributes(titleButton, menuWrapper, isExpanded);

    // Update the button description text based on the expansion state.
    updateButtonDescription(buttonDescription, isExpanded);
  }

  /**
   * Toggle the expansion state of a sidebar submenu.
   * @param {HTMLElement} target - The button element representing the sidebar submenu.
   */

  function sideBarMenuSelected(target) {
    const submenuButton = target;
    const submenuItems = target.nextElementSibling;
    const buttonDescription = target.querySelector('span');

    // Boolean indicating whether the menu is expanded or not.
    const isExpanded = target.getAttribute('aria-expanded') === 'true';

    // Toggle the aria-expanded and aria-hidden attributes.
    toggleAriaAttributes(submenuButton, submenuItems, isExpanded);

    // Update the button description text based on the expansion state.
    updateButtonDescription(buttonDescription, isExpanded);
  }

  /**
   * Toggle the aria-expanded and aria-hidden attributes.
   * @param {HTMLElement} arrowButton - The button element being interacted with in the sidebar menu.
   * @param {HTMLElement} menuList - The list being displayed or hidden.
   * @param {boolean} isExpanded - Boolean indicating whether the menu is expanded or not.
   */
  function toggleAriaAttributes(arrowButton, menuList, isExpanded) {
    // Toggle the expansion state of the button when clicked, setting aria-expanded to false if currently expanded (after the toggle).
    arrowButton.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');

    // Toggle the visibility state of the whole sidebar menu, setting aria-hidden to true if currently expanded (after the toggle).
    if (menuList) {
      menuList.setAttribute('aria-hidden', isExpanded ? 'true' : 'false');
    }
  }

  /**
   * Update the button description text based on the expansion state.
   * @param {HTMLElement} buttonDescription - The span element containing the description text.
   * @param {boolean} isExpanded - Boolean indicating whether the menu is expanded or not.
   */

  function updateButtonDescription(buttonDescription, isExpanded) {
    if (buttonDescription) {
      if (isExpanded) {
        buttonDescription.textContent = buttonDescription.textContent.replace(
          'Contract',
          'Expand',
        );
      } else {
        buttonDescription.textContent = buttonDescription.textContent.replace(
          'Expand',
          'Contract',
        );
      }
    }
  }
})();
