/* global $ */
/*eslint wrap-iife: [2, "inside"]*/

/**
 * This file adds the accordion functionality. It adds an inactiveClass to all headings.
 * Then it opens the first accordion element by adding an activeClass to it and adding the openClass
 * to it's sibling. We also make sure that we add correct aria- labels for accessiblity support. We add
 * an event listener to all headings so we can open/close the accordion content.
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 *
 * @summary Open/Close accordions
 *
 * @since 4.5.12
 */
(function (window) {
  'use strict';

  /**
   *  Main function
   * Sets default options and instatiates accordions.
   *
   * @param {Object} options  List of options to modify accordions
   */
  function tcuAccordions(options) {
    // default options
    var defaults = {
      target: '.tcu-accordion-container',
      accordionHeader: '.tcu-accordion-header',
      accordionContent: '.tcu-accordion-content',
      accordionWrapper: '.tcu-accordion',
      inactiveClass: 'tcu-inactive-header',
      activeClass: 'tcu-active-header',
      openClass: 'tcu-open-content',
      downArrowSVG: {
        width: 20,
        height: 20,
        src: "data:image/svg+xml,%3Csvg width='27' height='17' viewBox='0 0 27 17' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M25.831 2.468c0 .22-.075.44-.226.613l-10.798 12.4c-.288.33-.735.519-1.226.519s-.938-.19-1.226-.52L1.557 3.081a.946.946 0 0 1 .037-1.268.793.793 0 0 1 1.18.04l9.985 11.466c.445.512 1.2.512 1.644 0L24.39 1.854a.793.793 0 0 1 1.179-.041.928.928 0 0 1 .263.655' fill='%234D4D4A' stroke='%234D4D4A' stroke-width='1.96' fill-rule='evenodd'/%3E%3C/svg%3E",
      },
      upArrowSVG: {
        width: 20,
        height: 20,
        src: "data:image/svg+xml,%3Csvg width='27' height='17' viewBox='0 0 27 17' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1.169 14.532c0-.22.075-.44.226-.613l10.798-12.4c.288-.33.735-.519 1.226-.519s.938.19 1.226.52l10.798 12.399a.946.946 0 0 1-.037 1.268.793.793 0 0 1-1.18-.04L14.242 3.68a1.073 1.073 0 0 0-1.644 0L2.61 15.146a.793.793 0 0 1-1.179.041.928.928 0 0 1-.263-.655' fill='%234D4D4A' stroke='%234D4D4A' stroke-width='1.96' fill-rule='evenodd'/%3E%3C/svg%3E",
      },
    };

    // Combine defaults with user input
    options = extend(defaults, options);

    // Select accordion element
    const accordion = document.querySelectorAll(options.target);
    const accordionLength = accordion.length;

    // Bail if there's no accordion element
    if (accordionLength === 0) {
      return;
    }

    // Our variables for easier use
    const accordionParent = options.target;
    const accordionHeader = options.accordionHeader;
    const accordionContent = options.accordionContent;
    const inactiveClass = options.inactiveClass;
    const activeClass = options.activeClass;
    const openClass = options.openClass;
    const accordionWrapper = options.accordionWrapper;
    const firstHeading =
      accordionWrapper !== ''
        ? flattenArray(accordionWrapper + ':first-of-type ' + accordionHeader)
        : flattenArray(accordionHeader + ':first-of-type');
    const firstContent =
      accordionWrapper !== ''
        ? flattenArray(accordionWrapper + ':first-of-type ' + accordionContent)
        : flattenArray(accordionContent + ':first-of-type');
    const contentElements = flattenArray(accordionContent);
    const allHeadings = flattenArray(accordionHeader);

    // Up & Down Accordion Arrow sources
    const downArrowSVG = options.downArrowSVG;
    const upArrowSVG = options.upArrowSVG;

    // Add a polyfill for IE11 for .closest()
    closestPolyfill(true);

    // instatiate accordions
    init();

    /********************* Function Declarations ************************/

    /**
     * Init function
     *
     * @private
     */
    function init() {
      /**
       * Loop through accordion content and add aria-labels
       */
      for (let x = 0; x < contentElements.length; x++) {
        // Add aria labels
        if (
          !contentElements[x].classList.contains(
            'tcu-faqsearch-listitem__content',
          )
        ) {
          contentElements[x].setAttribute('aria-hidden', 'true');
          contentElements[x].setAttribute('aria-label', 'Accordion Content');
        }
      }

      /**
       * Loop through all headings and add an event click.
       */
      for (let x = 0; x < allHeadings.length; x++) {
        if (
          !allHeadings[x].classList.contains('tcu-faqsearch-listitem__title')
        ) {
          // add inactive class to headings
          allHeadings[x].classList.add(inactiveClass);

          // Add downArrow image tag.
          const downArrow = document.createElement('img');
          downArrow.height = downArrowSVG.height;
          downArrow.width = downArrowSVG.width;
          downArrow.alt = 'Open Accordion';
          downArrow.classList.add('tcu-down-arrow');
          downArrow.src = downArrowSVG.src;

          // Add img element to first heading.
          allHeadings[x].appendChild(downArrow);

          // Let screen readers know collapsable content below is in the expanded or in the collapsed state
          allHeadings[x].setAttribute('aria-expanded', 'false');

          // Add click event to all headings
          allHeadings[x].addEventListener('click', onClickListener);
        }
      }

      // If there's a hash that matches an element on the page, open that accordion
      if (
        window.location.hash &&
        document.querySelector(window.location.hash)
      ) {
        openHash();
      } else {
        // Open first accordion and add aria-expanded true to first headings
        for (let x = 0; x < firstHeading.length; x++) {
          if (
            firstHeading[x].dataset.open !== 'false' &&
            !firstHeading[x].classList.contains('tcu-faqsearch-listitem__title')
          ) {
            openAccordion(firstHeading[x], firstContent[x]);
          }
        }
      }

      window.addEventListener(
        'hashchange',
        function () {
          openHash();
        },
        false,
      );
    }

    function openHash() {
      var hashAccordion = document.querySelector(window.location.hash);
      if (
        hashAccordion &&
        hashAccordion.classList.contains(accordionHeader.substr(1)) &&
        hashAccordion.classList.contains(inactiveClass)
      ) {
        openAccordion(hashAccordion, hashAccordion.nextSibling);
      }
    }

    /**
     * Click event listener on all accordion headings
     *
     * @param {Node} event Click event target
     */
    function onClickListener(event) {
      let target = event.currentTarget;
      let sibling = target.nextElementSibling;

      if ('IMG' === target.nodeName) {
        target = event.target.parentNode;
        sibling = target.nextElementSibling;
      }

      if (target.classList.contains(inactiveClass)) {
        openAccordion(target, sibling);
        // Add url hash
        if (target.id) {
          // eslint-disable-next-line no-undef
          history.replaceState(null, null, `#${target.id}`);
        }
      } else if (target.classList.contains(activeClass)) {
        closeAccordion(target, sibling);
        // Remove url hash
        // eslint-disable-next-line no-undef
        history.replaceState(
          '',
          document.title,
          window.location.pathname + window.location.search,
        );
      }
    }

    /**
     * Displays the accordion content by replacing the inactiveClass.
     * It adds the openClass to sibling. This also changes the
     * aria-hidden to false.
     *
     * @param {Node} target  Click event target (should be the heading)
     * @param {Node} sibling Event's sibling (should be the content)
     */
    function openAccordion(target, sibling) {
      const currentImage = target.querySelector('img');
      const upArrow = document.createElement('img');
      upArrow.height = upArrowSVG.height;
      upArrow.width = upArrowSVG.width;
      upArrow.alt = 'Close Accordion';
      upArrow.src = upArrowSVG.src;

      // only open one accordion at a time
      let openElements = target
        .closest(accordionParent)
        .querySelectorAll('.' + activeClass); //tcu-active-header
      let openElementsCount = openElements.length;
      if (0 < openElementsCount) {
        for (let x = 0; x < openElementsCount; x++) {
          closeAccordion(openElements[x], openElements[x].nextElementSibling);
        }
      }

      // replace class of target
      target.classList.remove(inactiveClass);
      target.classList.add(activeClass);

      // Remove downArrow and replace with upArrow
      target.removeChild(currentImage);
      target.appendChild(upArrow);

      // Change the heading aria-expanded to true.
      target.setAttribute('aria-expanded', 'true');

      // Check if jQuery is added
      $(sibling).slideDown(300);

      // replace class for sibling
      sibling.classList.add(openClass);

      // change aria labels for sibling
      sibling.setAttribute('aria-hidden', 'false');
    }

    /**
     * Hides the accordion content by replacing the activeClass.
     * It removes the openClass from sibling. This also changes the
     * aria-hidden to true.
     *
     * @param {Node} target  Click event target (should be the heading)
     * @param {Node} sibling Event's sibling (should be the content)
     */
    function closeAccordion(target, sibling) {
      const currentImage = target.querySelector('img');
      const downArrow = document.createElement('img');
      downArrow.height = downArrowSVG.height;
      downArrow.width = downArrowSVG.width;
      downArrow.alt = 'Open Accordion';
      downArrow.src = downArrowSVG.src;

      // replace class of target
      target.classList.remove(activeClass);
      target.classList.add(inactiveClass);

      // Replace upArrow with downArrow
      target.removeChild(currentImage);
      target.appendChild(downArrow);

      // Change the heading aria-expanded to false.
      target.setAttribute('aria-expanded', 'false');

      // Check if jQuery is added
      $(sibling).slideUp(300);

      // replace class for sibling
      sibling.classList.remove(openClass);

      // change aria labels for sibling
      sibling.setAttribute('aria-hidden', 'true');
    }

    /**
     * Merge defaults with user options
     *
     * @private
     * @param   {Object} defaults Default settings
     * @param   {Object} options  User options
     * @returns {Object} Merged values of defaults and options
     */
    function extend(defaults, options) {
      const extended = {};
      for (const prop in defaults) {
        if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
          extended[prop] = defaults[prop];
        }
      }
      for (const prop in options) {
        if (Object.prototype.hasOwnProperty.call(options, prop)) {
          extended[prop] = options[prop];
        }
      }
      return extended;
    }

    /**
     * Push elements into an array and concat the multi level array.
     * This function loops through the accordion array.
     *
     * @param {String} querySelector String to use within querySelectorAll
     * @private
     * @return {Array} flatArray   One level array for easier access
     */
    function flattenArray(querySelector) {
      const items = [];

      // loop through the menu array and push found elements into items
      for (let x = 0; x < accordionLength; x++) {
        items.push(accordion[x].querySelectorAll(querySelector));
      }

      // let's flatten our flatArray array
      const flatArray = items.reduce(function (array, node) {
        for (let c = 0; c < node.length; c++) {
          array.push(node[c]);
        }
        return array;
      }, []);

      return flatArray;
    }

    /**
     * This is a polyfill from MDN for Element.closest() support in IE11.
     * The usePolyfill parameter is mostly to prevent uglifyJS from stripping
     * out the function.
     * https://developer.mozilla.org/en-US/docs/Web/API/Element/closest#polyfill
     * @param {Boolean} usePolyfill
     */
    function closestPolyfill(usePolyfill) {
      /* eslint-disable no-undef */
      if (usePolyfill === true) {
        if (!Element.prototype.matches) {
          Element.prototype.matches =
            Element.prototype.msMatchesSelector ||
            Element.prototype.webkitMatchesSelector;
        }

        if (!Element.prototype.closest) {
          Element.prototype.closest = function (s) {
            var el = this;

            do {
              if (Element.prototype.matches.call(el, s)) return el;
              el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType === 1);
            return null;
          };
        }
      }
      /* eslint-enable no-undef */
    }
  } // end of tcuAccordions();

  // add to global namespace
  window.tcuAccordions = tcuAccordions;
})(window);
