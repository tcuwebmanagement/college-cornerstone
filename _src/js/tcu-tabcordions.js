(function () {
  // Get all Tabcordions on the page
  const tabcordions = document.querySelectorAll('.tcu-tabcordion');
  const tabcordionWrapper = document.querySelectorAll(
    '.tcu-tabcordion__wrapper',
  );

  // For each tabcordion on the page, get the buttons and panels
  tabcordions.forEach((tabcordion) => {
    const tabcordionButtons = tabcordion.querySelectorAll('[role="tab"]');

    const tabButtonIndicators = Array.from(
      tabcordion.querySelectorAll('[aria-expanded]'),
    );

    const tabcordionPanels = Array.from(
      tabcordion.querySelectorAll('[role="tabpanel"]'),
    );

    /**
     * For checking if the screen size is small or large
     * @returns {string} small or large
     */
    function getSize() {
      let currentWidth =
        window.innerWidth || document.documentElement.clientWidth;
      return currentWidth >= 1000 ? 'large' : 'small';
    }

    /**
     * Remove all tab roles, and aria-selected attributes
     *
     */

    function removeRoles() {
      tabcordionWrapper.forEach((wrapper) => {
        wrapper.removeAttribute('role');
      });
      tabcordionButtons.forEach((button) => {
        button.removeAttribute('role');
        button.removeAttribute('aria-selected');
      });
      tabcordionPanels.forEach((panel) => {
        panel.removeAttribute('role');
      });
    }

    /**
     * Add all tab roles
     *
     */
    function addRoles() {
      tabcordionWrapper.forEach((button) => {
        button.setAttribute('role', 'tablist');
      });
      tabcordionButtons.forEach((button) => {
        button.setAttribute('role', 'tab');
      });
      tabcordionPanels.forEach((button) => {
        button.setAttribute('role', 'tabpanel');
      });
    }

    /**
     * Hide all tabpanels on pageload
     *
     */
    function hideAllPanels() {
      tabcordionPanels.forEach((tabcordionPanel) => {
        hidePanel(tabcordionPanel);
        tabcordionPanel.setAttribute('tabindex', '-1');
      });
      if (!(getSize() === 'small')) {
        setTabcordionButton('aria-selected', 'false');
      }
      setMobileIndicator('aria-expanded', 'false');
    }

    /**
     * Removes hidden attribute from the provided element and
     * adds visible class
     * @param {node} el
     */
    function showPanel(el) {
      el.classList.add('is-visible');
      el.setAttribute('tabindex', '0');
    }

    /**
     * Removes hidden attribute from the provided element and
     * adds visible class
     * @param {node} el
     */
    function hidePanel(el) {
      el.classList.remove('is-visible');
      el.setAttribute('tabindex', '-1');
    }

    /**
     * Set the attributes for the tabcordion buttons
     *
     * @param {string} attr
     * @param {boolean} value
     */
    function setTabcordionButton(attr, value) {
      // mark all tabs as unselected
      tabcordionButtons.forEach((tabcordionButton) => {
        tabcordionButton.setAttribute(attr, value);
      });
    }

    /**
     * Set the a visual indicator attribute value
     * for a target element
     *
     * @param {string} attr
     * @param {boolean} value
     */
    function setMobileIndicator(attr, value) {
      tabButtonIndicators.forEach((indicator) => {
        indicator.setAttribute(attr, value);
      });
    }

    /**
     * Returns a function, that, as long as it continues to be invoked, will not
     * be triggered. The function will be called after it stops being called for
     * N milliseconds. If `immediate` is passed, trigger the function on the
     * leading edge, instead of the trailing.
     * @param  {function} func
     * @param  {int} wait
     * @param  {boolean} immediate
     */
    function debounce(func, wait, immediate) {
      var timeout;
      return function () {
        var context = this,
          args = arguments;
        var later = function () {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        // eslint-disable-next-line no-undef
        clearTimeout(timeout);
        // eslint-disable-next-line no-undef
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }

    const handleResize = debounce(function () {
      if (getSize() === 'small') {
        //Remove the tab roles and aria-selected attributes when on mobile
        removeRoles();
        // On Mobile, move tab panel to be adjacent to tab buttons
        tabcordionButtons.forEach((button) => {
          button.insertAdjacentElement('afterend', getTabPanelById(button.id));
        });
      } else {
        // When larger than mobile, add the tab roles
        addRoles();
        // When larger than mobile, move tab panel to be after the tablist
        tabcordionButtons.forEach((button) => {
          tabcordion.insertAdjacentElement(
            'beforeend',
            getTabPanelById(button.id),
          );
        });
        // If no panel is open, open the first panel
        const openPanel = tabcordion.querySelector('.is-visible');
        if (!openPanel) {
          //Conditional to ignore if screen is smalll
          if (!(getSize() === 'small')) {
            tabcordionButtons[0].setAttribute('aria-selected', true);
          }
          tabcordionButtons[0].setAttribute('aria-expanded', true);
          showPanel(tabcordionPanels[0]);
        }
      }
    }, 100);

    // Link to open tabcordion
    //Open Tabcordion based on hash in URL
    var clickHash = function () {
      document.getElementById(window.location.hash.substring(1)).click();
    };

    /**
     * Return the first panel found in the Array of tabcordionPanels
     * that matches the id of the element that contains the aria labelled by
     * specific id
     *
     * @param {string} id the id for the tabpanel that matches the element clicked
     */
    function getTabPanelById(id) {
      return tabcordionPanels.find(
        (panel) => panel.getAttribute('aria-labelledby') === id,
      );
    }

    /**
     * Handles the display of the tabpanels associated with the event target
     * that is clicked and currently selected, and assigns boolean to
     * clicked event target (button)
     *
     * @param {*} event the event to emit
     */
    function handleTabCordions(event) {
      const target = event.currentTarget;
      const { id } = target;
      const tabcordionPanel = getTabPanelById(id);
      const hash = tabcordionPanel.getAttribute('aria-labelledby');
      const indicator = target;
      // If this panel is already open, return
      if (target.getAttribute('aria-expanded') === 'true') {
        // If this is mobile, close the tab
        if (getSize() === 'small') {
          hidePanel(tabcordionPanel);
          indicator.setAttribute('aria-expanded', false);
        }
        return;
      }

      // Reset panels
      hideAllPanels();

      // mark currently clicked tab as selected
      target.setAttribute('aria-selected', true);
      indicator.setAttribute('aria-expanded', true);

      // find associated tabpanel and show it
      if (tabcordionPanel.getAttribute('aria-labelledby') === id) {
        showPanel(tabcordionPanel);
        // eslint-disable-next-line no-undef
        history.replaceState(null, null, `#${hash}`);
        tabcordionPanel.focus();
      }
    }

    // Event Listeners

    // Show the first panel
    showPanel(tabcordionPanels[0]);

    // Click Event/s
    tabcordionButtons.forEach((tabcordionButton) =>
      tabcordionButton.addEventListener('click', handleTabCordions),
    );

    // Window Events
    window.addEventListener('resize', handleResize);
    window.addEventListener('hashchange', clickHash, false);
    //If there's a hash that matches an element on the page, open that accordion
    if (window.location.hash && document.querySelector(window.location.hash)) {
      clickHash();
    }

    // Call the resize event once for the inital pageload
    handleResize();
  });
})();
