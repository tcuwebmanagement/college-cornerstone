// eslint-disable-next-line no-unused-vars
class tcuSlideMenu {
  /**
   * A single slide menu element
   * @param  {Object} options.triggerElement the element that opens/closes the slide menu
   * @param  {Object} options.windowElement the element that contains the slide menu
   * @param  {string} options.hasChildrenSelector the selector string for elements that have children, expects an li
   * @param  {string} options.headerContainerClassName a class name for the header container
   * @param  {string} options.headerClassName a class name for the header text/link
   * @param  {string} options.backTriggerClassName a class name for the back button
   * @param  {string} options.backTriggerLabel a label for the back button
   * @param  {string} options.expandLabel an expand label for the dropdown button. use ${menuName} to insert the menu name
   * @param  {string} options.contractLabel a contract label for the dropdown button. use ${menuName} to insert the menu name
   * @param  {boolean} options.useSlide if the slide menu should use js applied slide transitions
   * @param  {boolean} options.useHeader if the slide menu should display a header in the header container
   * @param  {boolean} options.useBackTrigger if the slide menu should display a back button in the header container
   * @param  {boolean} options.addTriggerElementLabel if a label needs to be added to the triggerElement
   */
  constructor(options) {
    // Default options
    let defaults = {
      triggerElement: document.querySelector('.js-nav-toggle'),
      windowElement: document.querySelector('.nav-wrapper'),

      hasChildrenSelector: 'has-dropdown',
      headerContainerClassName: 'js-nav-header',
      headerClassName: 'js-nav-header-label',
      overflowClassName: 'js-nav-overflow-scroll',
      dropdownClassName: 'js-dropdown',
      backTriggerClassName: 'js-nav-back',
      backTriggerLabel: 'Back',
      expandLabel: 'Expand ${menuName} Menu',
      contractLabel: 'Contract ${menuName} Menu',

      backTriggerSVG:
        '<svg width="14" height="12" xmlns="http://www.w3.org/2000/svg"><path d="M1.041 5.477h10.09L7.334 1.82a.719.719 0 0 1-.246-.545.79.79 0 0 1 .22-.553.703.703 0 0 1 .536-.238c.21 0 .398.07.562.211l5.168 4.992a.728.728 0 0 1 .246.554.728.728 0 0 1-.246.554l-5.203 5.01a.754.754 0 0 1-.527.21.737.737 0 0 1-.563-.245.714.714 0 0 1-.22-.545.776.776 0 0 1 .238-.545l3.797-3.657H1.023c-.199 0-.375-.07-.527-.21a.73.73 0 0 1-.246-.51.748.748 0 0 1 .22-.58.765.765 0 0 1 .571-.246z" fill="#FFF" fill-rule="nonzero"/></svg>',
      hasChildrenSVG:
        '<svg width="14" height="12" xmlns="http://www.w3.org/2000/svg"><path d="M1.041 5.477h10.09L7.334 1.82a.719.719 0 0 1-.246-.545.79.79 0 0 1 .22-.553.703.703 0 0 1 .536-.238c.21 0 .398.07.562.211l5.168 4.992a.728.728 0 0 1 .246.554.728.728 0 0 1-.246.554l-5.203 5.01a.754.754 0 0 1-.527.21.737.737 0 0 1-.563-.245.714.714 0 0 1-.22-.545.776.776 0 0 1 .238-.545l3.797-3.657H1.023c-.199 0-.375-.07-.527-.21a.73.73 0 0 1-.246-.51.748.748 0 0 1 .22-.58.765.765 0 0 1 .571-.246z" fill="#FFF" fill-rule="nonzero"/></svg>',

      useSlide: true,
      useHeader: true,
      useBackTrigger: true,
      addTriggerElementLabel: true,
    };
    // Combine defaults with user options
    this.options = this.extend(defaults, options);

    // Add a unique id to this version of the slideMenu
    this.id = this.getUniqueID();

    // Keep track of current level and open/close status
    this.currentLevel = 0;
    this.isOpen = false;

    // Easy to use selectors
    this.triggerElement = this.options.triggerElement;
    this.windowElement = this.options.windowElement;
    this.parentElements = this.windowElement.querySelectorAll(
      `.${this.options.hasChildrenSelector}`,
    );

    // Bind all event handlers for referencability
    [
      'toggleWindow',
      'openSubMenu',
      'closeSubMenu',
      'handleKeydown',
      'handleOpenEvent',
    ].forEach((method) => {
      this[method] = this[method].bind(this);
    });

    // Keycodes for keydown events
    this.keyCode = {
      TAB: 9,
      RETURN: 13,
      ESC: 27,
      SPACE: 32,
      PAGEUP: 33,
      PAGEDOWN: 34,
      END: 35,
      HOME: 36,
      LEFT: 37,
      UP: 38,
      RIGHT: 39,
      DOWN: 40,
    };

    // Get started!
    this.init();
  }

  /**
   * Initializes the slide menu
   */
  init() {
    if (this.options.addTriggerElementLabel === true) {
      this.addTriggerLabel();
    }
    // Create Header Container
    if (
      this.options.useHeader === true ||
      this.options.useBackTrigger === true
    ) {
      this.createHeaderContainer();
      this.headerContainer = this.windowElement.querySelector(
        `.${this.options.headerContainerClassName}`,
      );
    }

    // Create Header Element
    if (this.options.useHeader === true) {
      this.createHeaderElements();
      this.headerElement = this.windowElement.querySelector(
        `.${this.options.headerClassName}`,
      );
    }

    // Create Back Buttons
    if (this.options.useBackTrigger === true) {
      this.createBackTriggerElements();
      this.backTriggerElement = this.windowElement.querySelector(
        `.${this.options.backTriggerClassName}`,
      );
    }

    // Add the arrow button to open submenus
    this.createDropdownButton();

    // Add level to windowElement
    this.windowElement.setAttribute('data-level', this.currentLevel);

    // Add Event Listeners
    this.addEventListeners();

    // Add accessible attributes to the menu
    this.addAriaAttributes();
    this.updateTabIndex();

    // Add some unique ids
    this.triggerElement.setAttribute('data-id', this.id);
    this.windowElement.setAttribute('data-id', this.id);
  }

  /**
   * Destroys the slide menu.
   * For rebuilding for different screen sizes with different needs
   */
  destroy() {
    // Remove Event Listeners and JS created elements
    this.removeEventListeners();
    if (
      this.options.useHeader === true ||
      this.options.useBackTrigger === true
    ) {
      this.removeHeaderContainer();
    }
    // Remove the arrow button to open submenus
    this.removeDropdownButton();

    // Remove Attributes
    this.triggerElement.removeAttribute('data-state');
    this.triggerElement.removeAttribute('data-id');
    this.windowElement.removeAttribute('data-state');
    this.windowElement.removeAttribute('data-id');
    this.windowElement.removeAttribute('data-level');

    // Remove tabindex
    this.removeTabIndex();

    // Remove active classes
    let openDropdowns = document.querySelectorAll('.nav-dropdown-open');
    for (let index = 0; index < openDropdowns.length; index++) {
      const element = openDropdowns[index];
      element.classList.remove('nav-dropdown-open');
    }

    // Remove styles
    this.windowElement.querySelector('ul').style = '';
    this.removeHeight();
  }

  /**
   * Adds all Event Listeners
   */
  addEventListeners() {
    // Create a custom event and listen for it
    // eslint-disable-next-line no-undef
    this.openEvent = new CustomEvent('open-slide-menu', { bubbles: true });
    document.addEventListener('open-slide-menu', this.handleOpenEvent);

    // Add click event listener for the open/close trigger element
    this.triggerElement.addEventListener('click', this.toggleWindow);

    // Add click listener for every parent element that has a dropdown
    for (let index = 0; index < this.parentElements.length; index++) {
      const element = this.parentElements[index];
      element
        .querySelector('button')
        .addEventListener('click', this.openSubMenu);
    }

    // Add click event listener for the back button
    if (this.options.useBackTrigger === true) {
      this.headerContainer.addEventListener('click', this.closeSubMenu);
    }

    // Add keydown listeners
    this.windowElement.addEventListener('keydown', this.handleKeydown);
    this.triggerElement.addEventListener('keydown', this.handleKeydown);
  }

  /**
   * Removes all Event Listeners
   */
  removeEventListeners() {
    // Remove custom event listener
    document.removeEventListener('open-slide-menu', this.handleOpenEvent);

    // Remove click event listener for the open/close trigger element
    this.triggerElement.removeEventListener('click', this.toggleWindow);

    // Remove click listener for every parent element that has a dropdown
    for (let index = 0; index < this.parentElements.length; index++) {
      const element = this.parentElements[index];
      element
        .querySelector('button')
        .removeEventListener('click', this.openSubMenu);
    }

    // Remove click event listener for the back button
    if (this.options.useHeader === true) {
      this.headerContainer.removeEventListener('click', this.closeSubMenu);
    }

    // Remove keydown listeners
    this.windowElement.removeEventListener('keydown', this.handleKeydown);
    this.triggerElement.removeEventListener('keydown', this.handleKeydown);
  }

  /**
   * Add the necessary aria attributes
   */
  addAriaAttributes() {
    this.triggerElement.setAttribute('aria-haspopup', true);
  }

  /**
   * Update tab indexes
   */
  updateTabIndex() {
    // Get open dropdowns
    const openDropdowns =
      this.windowElement.querySelectorAll('.nav-dropdown-open');
    let subMenu = [];

    // Reset all tabindexes
    const tabindexes = this.windowElement.querySelectorAll('a, button');
    for (let index = 0; index < tabindexes.length; index++) {
      const elem = tabindexes[index];
      elem.setAttribute('tabindex', -1);
    }

    // Check to see if there are children items
    if (openDropdowns.length >= 1) {
      let currentDropdown = openDropdowns[openDropdowns.length - 1];
      subMenu = currentDropdown.querySelector('.sub-menu').children;
      this.headerContainer.querySelector('button').setAttribute('tabindex', 0);
    } else if (this.isOpen && this.windowElement.querySelector('.tcu-menu')) {
      subMenu = this.windowElement.querySelector('.tcu-menu').children;
    } else if (this.isOpen) {
      subMenu = this.windowElement.querySelector('.sub-menu').children;
    }

    // Set the direct children of the subnav to tabindex 0
    for (let index = 0; index < subMenu.length; index++) {
      const element = subMenu[index];
      element.querySelector('a').setAttribute('tabindex', 0);
      if (element.classList.contains(this.options.hasChildrenSelector)) {
        element.querySelector('button').setAttribute('tabindex', 0);
      }
    }
  }
  /**
   * Remove tab indexes attributes
   */
  removeTabIndex() {
    const tabindexes = this.windowElement.querySelectorAll('a, button');
    for (let index = 0; index < tabindexes.length; index++) {
      const elem = tabindexes[index];
      elem.removeAttribute('tabindex');
    }
  }

  /**
   * Toggles the this.windowElement and updates the state
   */
  toggleWindow() {
    this.isOpen = !this.isOpen;

    if (this.isOpen) {
      this.triggerElement.dispatchEvent(this.openEvent);
    }

    // Toggle data-state attribute for the triggerElement and the windowElement
    this.toggleState(this.triggerElement, '', 'open');
    this.toggleState(this.triggerElement.parentNode, '', 'open');
    this.toggleState(this.windowElement, '', 'open');

    // Update the data-level attribute so we know what the current level is
    this.windowElement.setAttribute('data-level', this.currentLevel);

    if (this.isOpen) {
      // Toggle aria-expanded attribute
      this.triggerElement.setAttribute('aria-expanded', true);
      // Update windowElement height
      this.setHeight();
    } else {
      // Toggle aria-expanded attribute
      this.triggerElement.removeAttribute('aria-expanded');
      this.triggerElement.focus();
      // Remove windowElement height
      this.removeHeight();
    }

    if (this.options.addTriggerElementLabel === true) {
      this.toggleTriggerLabel();
    }

    // Update tab indexes
    this.updateTabIndex();
  }

  /**
   * Closes this.windowElement and updates the state
   */
  closeWindow() {
    this.isOpen = false;

    // Toggle data-state attribute for the triggerElement and the windowElement
    this.toggleState(this.triggerElement, '', '');
    this.toggleState(this.triggerElement.parentNode, '', '');
    this.toggleState(this.windowElement, '', '');

    // Update the data-level attribute so we know what the current level is
    this.windowElement.setAttribute('data-level', 0);

    // Toggle aria-expanded attribute
    this.triggerElement.removeAttribute('aria-expanded');

    // Remove windowElement height
    this.removeHeight();

    // Update tab indexes
    this.updateTabIndex();
  }

  /**
   * Updates the current level
   * @param  {int} direction
   */
  updateLevel(direction) {
    this.currentLevel += direction;
  }

  /**
   * Updates the header text
   * @param  {string} text
   */
  updateHeaderText(text) {
    // Check if we're using headers
    if (this.options.useHeader === false) {
      return;
    }
    this.headerElement.innerText = text;
  }

  // -------------------------  Events ----------------------------------------

  /**
   * Control what happens on keydown
   * For triggerElement and windowElement
   * @param  {} event
   */
  handleKeydown(event) {
    switch (event.keyCode) {
      case this.keyCode.ESC:
        // Close the whole menu if we're at level 0, else just go down a level
        if (this.currentLevel === 0 && this.isOpen) {
          this.toggleWindow();
        } else if (this.isOpen) {
          this.closeSubMenu();
        }
        break;
    }
  }

  /**
   * Opens the submenu of a slide menu
   * Increases current level, uses transitions, updates classes, and updates headers
   * @param  {} event
   */
  openSubMenu(event) {
    event.preventDefault();

    // Get current menu item
    const menuItem = event.currentTarget.previousSibling;

    // Increase the current level
    this.updateLevel(1);

    // Update nav header
    this.updateHeaderText(menuItem.firstChild.textContent);

    // Add css classes
    menuItem.parentNode.classList.add('nav-dropdown-open');
    this.windowElement.setAttribute('data-level', this.currentLevel);

    // Slide the menu to the appropriate level
    if (this.options.useSlide) {
      this.slideMenu();
    }

    // Update the height of the windowElement so that you can go crazy with the
    // amount of nav items
    this.setHeight();

    this.focusHeader();

    // Update tab indexes
    this.updateTabIndex();
  }

  /**
   * Closes the submenu of a slide menu
   * Decreases current level, uses transitions, updates classes, and updates headers
   * @param  {} event
   */
  closeSubMenu() {
    // Get current menu item
    const menuItems = this.windowElement.querySelectorAll('.nav-dropdown-open');
    const menuItem = menuItems[menuItems.length - 1];
    const prevMenuItem = menuItems[menuItems.length - 2];

    // Return if we're already at the lowest level
    if (this.currentLevel === 0) {
      return;
    }

    // Decrease the current level
    this.updateLevel(-1);

    // Update nav header
    if (this.currentLevel === 0) {
      this.updateHeaderText('');
    } else {
      this.updateHeaderText(
        prevMenuItem.querySelector('a').firstChild.textContent,
      );
    }

    // Update css classes
    menuItem.classList.remove('nav-dropdown-open');
    this.windowElement.setAttribute('data-level', this.currentLevel);

    // Slide the menu to the appropriate level
    if (this.options.useSlide) {
      this.slideMenu();
    }

    // Update the height of the windowElement so that you can go crazy with the
    // amount of nav items
    this.setHeight();

    // Add focus to current menu item
    // preventScroll is not supported by iOS
    menuItem.querySelector('a').focus({ preventScroll: 0 });
    // eslint-disable-next-line no-undef
    setTimeout(() => {
      // eslint-disable-next-line no-console
      if (this.windowElement.scrollTop !== 0) {
        this.windowElement.scrollTop = 0;
      }
    }, 400);

    // Update tab indexes
    this.updateTabIndex();
  }

  /**
   * Handles the open event so that only one instance of the slide menu is open at a time
   * @param  {} event
   */
  handleOpenEvent(event) {
    let selectedID = event.target.getAttribute('data-id');
    if (selectedID !== this.id && this.isOpen) {
      this.closeWindow();
    }
  }

  // -------------------------  Focus -----------------------------------------

  focusHeader() {
    this.headerContainer.querySelector('button').focus();
  }

  // ------------------------- JS Created Elements ----------------------------

  addTriggerLabel() {
    // Create Label
    if (!this.triggerElement.querySelector('.show-for-sr')) {
      let label = document.createElement('span');
      let menuName = this.triggerElement.previousElementSibling.text;
      label.className = 'show-for-sr';
      label.innerText = this.options.expandLabel.replace(
        '${menuName}',
        menuName,
      );
      this.triggerElement.appendChild(label);
    }
  }

  toggleTriggerLabel() {
    let label = this.triggerElement.querySelector('.show-for-sr');
    let menuName = this.triggerElement.previousElementSibling.text;
    if (this.isOpen) {
      label.innerText = this.options.contractLabel.replace(
        '${menuName}',
        menuName,
      );
    } else {
      label.innerText = this.options.expandLabel.replace(
        '${menuName}',
        menuName,
      );
    }
  }

  /**
   * Creates a header container element and prepends it to the this.windowElement
   * This will contain the header or the back buttons
   */
  createHeaderContainer() {
    // Check if we're using headers or back buttons
    if (
      this.options.useHeader === false &&
      this.options.useBackTrigger === false
    ) {
      return;
    }
    let header = document.createElement('div');
    header.className = this.options.headerContainerClassName;

    this.windowElement.insertBefore(header, this.windowElement.firstChild);
  }

  /**
   * Removes a header container element
   */
  removeHeaderContainer() {
    // Check if we're using headers or back buttons
    if (
      this.options.useHeader === false &&
      this.options.useBackTrigger === false
    ) {
      return;
    }
    let header = this.windowElement.querySelector(
      `.${this.options.headerContainerClassName}`,
    );
    header.remove();
  }

  /**
   * Creates a header container element and prepends it to the this.headerContainer
   * This header is for the current submenu level
   */
  createHeaderElements() {
    // Check if we're using headers
    if (this.options.useHeader === false) {
      return;
    }
    let header = document.createElement('div');
    header.className = this.options.headerClassName;
    this.headerContainer.insertBefore(header, this.headerContainer.firstChild);
  }

  /**
   * Removes a header container element
   */
  removeHeaderElements() {
    // Check if we're using headers
    if (this.options.useHeader === false) {
      return;
    }
    let header = this.headerContainer.querySelector(
      `.${this.options.headerClassName}`,
    );
    header.remove();
  }

  /**
   * Creates a back button and prepends it to the this.headerContainer
   * This button is for going back up a level in the submenu
   */
  createBackTriggerElements() {
    // Check if we're using back buttons
    if (this.options.useBackTrigger === false) {
      return;
    }
    let button = document.createElement('button');
    button.className = this.options.backTriggerClassName;
    button.innerHTML = this.options.backTriggerSVG;

    let label = document.createElement('span');
    label.className = 'show-for-sr';
    label.innerText = this.options.backTriggerLabel;

    button.appendChild(label);

    this.headerContainer.insertBefore(button, this.headerContainer.firstChild);
  }

  /**
   * Removes a back button
   */
  removeBackTriggerElements() {
    // Check if we're using back buttons
    if (this.options.useBackTrigger === false) {
      return;
    }
    let button = this.headerContainer.querySelector(
      `.${this.options.backTriggerClassName}`,
    );
    button.remove();
  }

  /**
   * Creates the arrow button that opens submenus
   */
  createDropdownButton() {
    for (let index = 0; index < this.parentElements.length; index++) {
      const parent = this.parentElements[index];
      // Create Button
      let button = document.createElement('button');
      button.className = this.options.dropdownClassName;
      button.innerHTML = this.options.hasChildrenSVG;
      // Create Label
      let label = document.createElement('span');
      let menuName = parent.querySelector('a').text;
      label.className = 'show-for-sr';
      label.innerText = this.options.expandLabel.replace(
        '${menuName}',
        menuName,
      );
      button.appendChild(label);
      // Append Button
      parent.querySelector('a').insertAdjacentElement('afterend', button);
    }
  }
  /**
   * Removes the arrow button that opens submenus
   */
  removeDropdownButton() {
    for (let index = 0; index < this.parentElements.length; index++) {
      const parent = this.parentElements[index];
      parent.querySelector(`.${this.options.dropdownClassName}`).remove();
    }
  }

  // -------------------------  Transitions ---------------------------------

  /**
   * Slide the main menu based on current menu depth
   *
   * @param {int} level the level that we want to slide transition to animate to
   */
  slideMenu(level) {
    let translate = level || this.currentLevel;
    this.windowElement.querySelector('ul').style.transform = `translateX(-${
      translate * 100
    }%)`;
  }
  /**
   * Set the height of the windowElement to match the height of the currently open dropdown
   */
  setHeight() {
    const menuItems = this.windowElement.querySelectorAll('.nav-dropdown-open');
    const mainMenu = this.windowElement.querySelector('ul');
    const maxHeight = parseInt(
      window.getComputedStyle(this.windowElement)['max-height'],
    );

    this.windowElement.classList.remove(this.options.overflowClassName);
    if (menuItems.length) {
      const menuItem = menuItems[menuItems.length - 1].querySelector('ul');
      let height = menuItem.offsetHeight + this.headerContainer.offsetHeight;

      this.windowElement.style.height = `${height}px`;

      // Only add overflow class if the menu is taller than the max height of the windowElement
      if (height > maxHeight) {
        this.windowElement.classList.add(this.options.overflowClassName);
      }
    } else {
      this.windowElement.style.height = `${mainMenu.offsetHeight}px`;

      // Only add overflow class if the main menu is taller than the max height of the windowElement
      if (mainMenu.offsetHeight > maxHeight) {
        this.windowElement.classList.add(this.options.overflowClassName);
      }
    }
  }

  /**
   * Removes the height of the windowElement
   */
  removeHeight() {
    this.windowElement.style.height = ``;
    this.windowElement.classList.remove(this.options.overflowClassName);
  }

  // -------------------------  Helpers -------------------------------------

  /**
   * Toggle State Function
   * @author Nicholas Tillman
   * @date 2019-03-26 15:04:15
   * @desc Toggles the state for our menu and search
   * non-core sites
   *
   * @param {Object} elem the element to set data attribute
   * @param {string} oldValue default value of the element state
   * @param {string} newValue new value of the element state
   */
  toggleState(elem, oldValue, newValue) {
    const dataState = elem.getAttribute('data-state');
    elem.setAttribute(
      'data-state',
      dataState === oldValue || dataState === null ? newValue : oldValue,
    );
  }

  /**
   * Merge defaults with user options
   *
   * @param {Object} defaults Default settings
   * @param {Object} options  User options
   * @returns {Object} Merged values of defaults and options
   */
  extend(defaults, options) {
    let extended = {};
    let prop;
    for (prop in defaults) {
      if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
        extended[prop] = defaults[prop];
      }
    }
    for (prop in options) {
      if (Object.prototype.hasOwnProperty.call(options, prop)) {
        extended[prop] = options[prop];
      }
    }
    return extended;
  }

  getUniqueID() {
    return Math.random().toString(36).substr(2, 9);
  }
}

/**
 * Polyfill for Custom Events
 */
(function () {
  if (typeof window.CustomEvent === 'function') return false;

  function CustomEvent(event, params) {
    params = params || { bubbles: false, cancelable: false, detail: null };
    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(
      event,
      params.bubbles,
      params.cancelable,
      params.detail,
    );
    return evt;
  }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;
})();
