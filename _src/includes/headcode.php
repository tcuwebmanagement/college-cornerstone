<!-- favicons -->
<link rel="apple-touch-icon" href="/_resources/images/apple-icon-touch.png">
<link rel="icon" href="/_resources/images/favicon.png">
<!--[if IE]>
<link rel="shortcut icon" href="/_resources/images/favicon.ico">
<![endif]-->
<meta name="msapplication-TileColor" content="#4d1979">
<meta name="msapplication-TileImage" content="/_resources/images/win8-tile-icon.png">

<!-- js -->
<script src="https://assets.tcu.edu/js/libs/modernizr.custom.min.js"></script>

<!-- CSS & fonts -->
<link rel="stylesheet" href="/_resources/css/style.min.css" type="text/css" media="all">

<!-- alerts -->
<link rel="stylesheet" href="https://assets.tcu.edu/emergency-alerts/emergency-alerts.css">

<?php include_once 'global-head.php'; ?>
