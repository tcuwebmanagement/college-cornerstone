<?php include_once 'global-nav.php'; ?>
<!-- jquery -->
<script src="//assets.tcu.edu/js/libs/jquery.min.js"></script>

<!-- dependencies -->
<!-- fact-banner,fact-slider,image-gallery,slider -->
<script type="text/javascript" src="//assets.tcu.edu/js/libs/flickity.pkgd.min.js"></script>
<!-- video,hero,title-card,title-card--large-buttons -->
<script src="https://player.vimeo.com/api/player.js"></script>

<!-- component js -->
<script type="text/javascript" src="//assets.tcu.edu/cornerstone/0.0.0/js/tcu-accordions.min.js"></script>
<script type="text/javascript" src="//assets.tcu.edu/cornerstone/0.0.0/js/tcu-slide-menu.min.js"></script>
<script type="text/javascript" src="//assets.tcu.edu/cornerstone/0.0.0/js/tcu-tabcordions.min.js"></script>
<script type="text/javascript" src="//assets.tcu.edu/cornerstone/0.0.0/js/tcu-video-showcase.min.js"></script>

<!-- scripts -->
<script type="text/javascript" src="//assets.tcu.edu/cornerstone/0.0.0/scripts.min.js"></script>

<!-- alerts -->
<script src="//assets.tcu.edu/emergency-alerts/emergency-alerts.min.js"></script>
<script>
	$('body').OUAlert({
		type       : 'active',
		activePath : '/ou-alerts/active-alerts.xml',
		icon       : false,
		emergency  : {
			class           : 'tcu-alert _emergency',
			position        : 'top', //Positions are ‘top’, ‘bottom’, ‘modal’
			modalSize       : 'large', //Determines width of the modal if `position` is 'modal' (large, medium, small)
			fontColor       : '', //Optional Ex: '#333', sets font color
			backgroundColor : '#FFC5C0', //Optional Ex: 'blue', sets background color
			buttonClass     : '', //Optional Ex: 'btn btn-primary', adds extra class names to button element
			buttonText      : '', //Optional Ex: 'Dismiss', Button text defaults to 'Close' if not specified
		},
		warning  : {
			class           : 'tcu-alert _warning',
			position        : 'top',
			fontColor       : '',
			backgroundColor : '#FFE5C0',
			buttonClass     : '',
			buttonText      : '',
		},
		announcement  : {
			class           : 'tcu-alert _announce',
			position        : 'top',
			fontColor       : '',
			backgroundColor : '#B3CFE8',
			buttonClass     : '',
			buttonText      : '',
		}
	});
	$('#archived-alerts-div').OUAlert({
		type       : 'archived',
		archivedPath : '/ou-alerts/archived-alerts.xml',
		popup      : false,
		archiveHeader : false
	});
</script>
