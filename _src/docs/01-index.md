<img src="https://assets.tcu.edu/cornerstone/images/800x400-cornerstone-mark.jpg" alt="Cornerstone" width="500" height="250"/>

## Mission

TCU Cornerstone is the university’s design system. A design system is the single source of truth containing elements that help teams design, conceptualize, and develop products and marketing websites that deliver satisfying and consistent experiences to users.

## Vision

Design systems are not experimental. They are backed by research on how users behave and what they want. Once you have solidified successful UI patterns, you want to document them and stick to them.

However, design systems are built to evolve over time. No design system ever stays the same for a very long time.

Would you like to request a new pattern or improvement? <a href="mailto:webmanage@tcu.edu">Let us know</a>

## Benefits

### 1. Improved collaboration

By implementing a design system that allows your teams to create simple websites with ease, designers and developers can focus on refining current patterns and new components that will add more value than repetitive work ever could.

### 2. Consistent and better user experiences

When you have a centralized system that contains preapproved components and assets, marketers can create web pages and user interfaces without compromising visual consistency, resulting in solutions that create better experiences.

### 3. Empowers content experts to be in control of their content

Having a design system allows stakeholders and content creators to maintain their web presence, without always going through web management to make simple updates and add modules. We are always available to help, but the success of a design system is when multiple teams can use it to meet their goals and keep their content fresh!

### 4. Better efficiency and reduced expenses

A design system contains reusable assets and components that marketers, designers, and developers can use to create web pages on the fly. No one has to create anything from scratch because everything they need is within the design system.
