{% set totalComponents = getComponents().length %}
{% set totalComponents = getComponents().length %}
{% set completedComponents = getComponents("Complete").length %}

<progress max="{{ totalComponents }}" value="{{ completedComponents }}"> {{ completedComponents }} out of {{ totalComponents }} </progress>

{% if getComponents("In Progress").length > 0 %}

## In Progress

  <ul>
  {% for component in getComponents("In Progress") %}
    <li><a href="{{ ('/components/detail/' ~ component.handle) | path}}">{{ component.title }}</a></li>
  {% endfor %}
  </ul>
{% endif %}

{% if getComponents("refactor").length > 0 %}

## Needs Refactor

<ul>
{% for component in getComponents("refactor") %}
  <li><a href="{{ ('/components/detail/' ~ component.handle) | path}}">{{ component.title }}</a></li>
{% endfor %}
</ul>
{% endif %}

## Not Ready

<ul>
{% for component in getComponents("Not Ready") %}
  <li><a href="{{ ('/components/detail/' ~ component.handle) | path}}">{{ component.title }}</a></li>
{% endfor %}
</ul>

## Complete

<ul>
{% for component in getComponents("Complete") %}
  <li><a href="{{ ('/components/detail/' ~ component.handle) | path}}">{{ component.title }}</a></li>
{% endfor %}
</ul>

## Deprecated

<ul>
{% for component in getComponents("Deprecated") %}
  <li><a href="{{ ('/components/detail/' ~ component.handle) | path}}">{{ component.title }}</a></li>
{% endfor %}
</ul>
