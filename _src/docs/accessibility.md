Our target accessibility standard is the [World Wide Web Consortium (W3C) Web Content Accessibility Guidelines (WCAG) 2.1 Level AA](https://www.w3.org/TR/WCAG21/).

## Use semantic HTML

- If you need a button, use `<button>`
- Use `<strong>` instead of `<b>`
- Use `<em>` instead of `<i>`
