{% for component in getComponents() %}

  <div class="Pen-panel Pen-header" style="padding-left: 0;">
  <h1>{{ component.title }}</h1>
  <div class="Tags">
  {% for tag in component.tags %}
      {% if tag == "ou-component" %}<label class="Status-label" style="background-color: #0271a5; border-color: #0271a5;">{{ tag | replace({'ou-': ''}) }}</label>{% endif %}
      {% if tag == "ou-snippet" %}<label class="Status-label" style="background-color: #0271a5; border-color: #0271a5;">{{ tag | replace({'ou-': ''}) }}</label>{% endif %}
      {% if tag == "ou-system" %}<label class="Status-label" style="background-color: #0271a5; border-color: #0271a5;">{{ tag | replace({'ou-': ''}) }}</label>{% endif %}
      {% if tag == "wordpress-block" %}<label class="Status-label" style="background-color: #00a0d2; border-color: #00a0d2;">{{ tag | replace({'wordpress-': ''}) }}</label>{% endif %}
  {% endfor %}
  </div>
  </div>

{{ component.notes }}

  <iframe class="_large" data-role="window" src="{{ ('/components/preview/' ~ component.handle) | path}}" loading="lazy" sandbox="allow-same-origin allow-scripts allow-forms allow-modals" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" scrolling="yes" loading="lazy"></iframe>
{% endfor %}
