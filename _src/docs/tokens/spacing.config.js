'use strict';

const path = require('path');

module.exports = {
  context: {
    spacing: require(path.join(process.cwd(), '_src/tokens/spacing.json')),
  },
};
