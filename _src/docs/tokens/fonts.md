<h2>Typography</h2>
<table>
  <thead>
    <tr>
      <th>Example</th>
      <th>Description</th>
      <th>Token</th>
    </tr>
  </thead>
  <tbody>
    <!-- stats -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.stats_font_size.value }};
          font-weight: {{ fonts.stats_font_weight.value }};
          font-family: {{ fonts.stats_font_family.value }}"
        >Stats</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>stats</h2>
        <p>font-size: {{ fonts.stats_font_size.value }}</p>
        <p>font-weight: {{ fonts.stats_font_weight.value }}</p>
        <p>font-family: {{ fonts.stats_font_family.value }}</p>
      </td>
      <td>
        <code>$stats-font-size</code>
        <code>$stats-font-weight</code>
        <code>$stats-font-family</code>
      </td>
    </tr>
    <!-- xxlarge -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.xxlarge_font_size.value }};
          font-weight: {{ fonts.xxlarge_font_weight.value }};
          font-family: {{ fonts.xxlarge_font_family.value }}"
        >Heading 1</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>Heading 1 - xxlarge</h2>
        <p>font-size: {{ fonts.xxlarge_font_size.value }}</p>
        <p>font-weight: {{ fonts.xxlarge_font_weight.value }}</p>
        <p>font-family: {{ fonts.xxlarge_font_family.value }}</p>
      </td>
      <td>
        <code>$xxlarge-font-size</code>
        <code>$xxlarge-font-weight</code>
        <code>$xxlarge-font-family</code>
      </td>
    </tr>
    <!-- xlarge -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.xlarge_font_size.value }};
          font-weight: {{ fonts.xlarge_font_weight.value }};
          font-family: {{ fonts.xlarge_font_family.value }}"
        >Heading 2</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>Heading 2 - xlarge</h2>
        <p>font-size: {{ fonts.xlarge_font_size.value }}</p>
        <p>font-weight: {{ fonts.xlarge_font_weight.value }}</p>
        <p>font-family: {{ fonts.xlarge_font_family.value }}</p>
      </td>
      <td>
        <code>$xlarge-font-size</code>
        <code>$xlarge-font-weight</code>
        <code>$xlarge-font-family</code>
      </td>
    </tr>
    <!-- large -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.large_font_size.value }};
          font-weight: {{ fonts.large_font_weight.value }};
          font-family: {{ fonts.large_font_family.value }}"
        >Heading 3</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>Heading 3 - large</h2>
        <p>font-size: {{ fonts.large_font_size.value }}</p>
        <p>font-weight: {{ fonts.large_font_weight.value }}</p>
        <p>font-family: {{ fonts.large_font_family.value }}</p>
      </td>
      <td>
        <code>$large-font-size</code>
        <code>$large-font-weight</code>
        <code>$large-font-family</code>
      </td>
    </tr>
    <!-- medium -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.medium_font_size.value }};
          font-weight: {{ fonts.medium_font_weight.value }};
          font-family: {{ fonts.medium_font_family.value }}"
        >Heading 4</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>Heading 4 - medium</h2>
        <p>font-size: {{ fonts.medium_font_size.value }}</p>
        <p>font-weight: {{ fonts.medium_font_weight.value }}</p>
        <p>font-family: {{ fonts.medium_font_family.value }}</p>
      </td>
      <td>
        <code>$medium-font-size</code>
        <code>$medium-font-weight</code>
        <code>$medium-font-family</code>
      </td>
    </tr>
    <!-- small -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.small_font_size.value }};
          font-weight: {{ fonts.small_font_weight.value }};
          font-family: {{ fonts.small_font_family.value }}"
        >Heading 5</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>Heading 5 - small</h2>
        <p>font-size: {{ fonts.small_font_size.value }}</p>
        <p>font-weight: {{ fonts.small_font_weight.value }}</p>
        <p>font-family: {{ fonts.small_font_family.value }}</p>
      </td>
      <td>
        <code>$small-font-size</code>
        <code>$small-font-weight</code>
        <code>$small-font-family</code>
      </td>
    </tr>
    <!-- xsmall -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.xsmall_font_size.value }};
          font-weight: {{ fonts.xsmall_font_weight.value }};
          font-family: {{ fonts.xsmall_font_family.value }}"
        >Heading 6</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>Heading 6 - xsmall</h2>
        <p>font-size: {{ fonts.xsmall_font_size.value }}</p>
        <p>font-weight: {{ fonts.xsmall_font_weight.value }}</p>
        <p>font-family: {{ fonts.xsmall_font_family.value }}</p>
      </td>
      <td>
        <code>$xsmall-font-size</code>
        <code>$xsmall-font-weight</code>
        <code>$xsmall-font-family</code>
      </td>
    </tr>
    <!-- hammerhead -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.hammerhead_font_size.value }};
          font-weight: {{ fonts.hammerhead_font_weight.value }};
          font-family: {{ fonts.hammerhead_font_family.value }}"
        >Hammerhead</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>hammerhead</h2>
        <p>font-size: {{ fonts.hammerhead_font_size.value }}</p>
        <p>font-weight: {{ fonts.hammerhead_font_weight.value }}</p>
        <p>font-family: {{ fonts.hammerhead_font_family.value }}</p>
      </td>
      <td>
        <code>$hammerhead-font-size</code>
        <code>$hammerhead-font-weight</code>
        <code>$hammerhead-font-family</code>
      </td>
    </tr>
    <!-- body -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.body_font_size.value }};
          font-weight: {{ fonts.body_font_weight.value }};
          font-family: {{ fonts.body_font_family.value }}"
        >Body</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>body</h2>
        <p>font-size: {{ fonts.body_font_size.value }}</p>
        <p>font-weight: {{ fonts.body_font_weight.value }}</p>
        <p>font-family: {{ fonts.body_font_family.value }}</p>
      </td>
      <td>
        <code>$body-font-size</code>
        <code>$body-font-weight</code>
        <code>$body-font-family</code>
      </td>
    </tr>
    <!-- caption -->
    <tr>
      <td>
        <p style="
          font-size: {{ fonts.caption_font_size.value }};
          font-weight: {{ fonts.caption_font_weight.value }};
          font-family: {{ fonts.caption_font_family.value }}"
        >Caption</p>
        <p>{{ variable.value }}</p>
      </td>
      <td>
        <h2>caption</h2>
        <p>font-size: {{ fonts.caption_font_size.value }}</p>
        <p>font-weight: {{ fonts.caption_font_weight.value }}</p>
        <p>font-family: {{ fonts.caption_font_family.value }}</p>
      </td>
      <td>
        <code>$caption-font-size</code>
        <code>$caption-font-weight</code>
        <code>$caption-font-family</code>
      </td>
    </tr>
  </tbody>
</table>

<h2>Font Sizes</h2>
<table>
  <thead>
    <tr>
      <th>Example</th>
      <th>Description</th>
      <th>Token</th>
    </tr>
  </thead>
  <tbody>
    {% for key, variable in fonts %}
      {% if variable.category == 'font-size' %}
        <tr>
          <td>
            <p style="font-size: {{ variable.value }}">Aa Bb Cc</p>
            <p>{{ variable.value }}</p>
          </td>
          <td>
            <h2>{{ key | titlecase('_') }}</h2>
          </td>
          <td>
            <code>${{ key | replace({'_':'-'}) }}</code>
          </td>
        </tr>
      {% endif %}
    {% endfor %}
  </tbody>
</table>
