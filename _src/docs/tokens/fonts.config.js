'use strict';

const path = require('path');
const fonts = require(path.join(process.cwd(), '_src/tokens/text-styles.json'));

module.exports = {
  context: {
    fonts: fonts.props,
  },
};
