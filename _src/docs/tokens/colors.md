{% for key, color in colors %}

<table>
  <thead>
    <tr>
      <th width="200">Example</th>
      <th>Description</th>
      <th>Token</th>
    </tr>
  </thead>
  <tbody>
    {% for key, variable in color %}
      <tr>
        <td>
          <div class="radius-box" style="background-color: {{ variable.value }};"></div>
          <p>{{ variable.value }}</p>
        </td>
        <td>
          <h2>{{ key | titlecase('_') }}</h2>
        </td>
        <td>
          <code>${{ key | replace({'_':'-'}) }}</code>
          <code>var(--color-{{ key | replace({'_':'-'}) }})</code>
        </td>
      </tr>
    {% endfor %}
  </tbody>
</table>
{% endfor %}
