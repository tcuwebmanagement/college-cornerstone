{% for key, space in spacing %}

<table>
  <thead>
    <tr>
      <th width="200">Example</th>
      <th>Description</th>
      <th>Token</th>
    </tr>
  </thead>
  <tbody>
    {% for key, variable in space %}
      <tr>
        <td>
          <div style="background: grey; height: {{ variable.value }}; width: {{ variable.value }};"></div>
          <p>{{ variable.value }}</p>
        </td>
        <td>
          <h2>{{ key | titlecase('_') }}</h2>
        </td>
        <td>
          <code>${{ key | replace({'_':'-'}) }}</code>
        </td>
      </tr>
    {% endfor %}
  </tbody>
</table>
{% endfor %}
