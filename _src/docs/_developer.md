## Pattern Library Structure

Proposed library structure to keep us organized.

<!-- prettier-ignore-start -->
```markdown
components/     # Patterns
├── base/       # …that are basic building blocks like HTML tags
├── common/     # …that may appear anywhere
├── global/     # …that appear on every page
├── templates/  # …that combine patterns to render page types
└── utilities/  # …helpers that might not have visual representation
```
<!-- prettier-ignore-end -->

## Sass Structure

The style.scss follows Inverted Triangle CSS (ITCSS) structure to keep organized.
The idea behind ITCSS is that you go from generic to more specific. For more information,
read the descriptions in style.scss and check out the following articles:

- https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/
- https://www.creativebloq.com/web-design/manage-large-css-projects-itcss-101517528

## Foundation

Cornerstone makes use of the Foundation front-end framework.

### Configuring Foundation

\_src/scss/foundation/\_foundation.scss
To edit configure which parts of Foundation are used

\_src/scss/foundation/\_settings.scss
To change the settings for Foundation

## Configuring Cornerstone

### Tokens

One of the most significant ways to alter Cornerstone is by using different design tokens.
We use a sketch file along with the [Design Token Exporter](https://github.com/here-erhe/Design-Token-Exporter) plugin to export tokens to json files.

### Config

\_src/scss/settings/\_config.scss
To change settings for Cornerstone
