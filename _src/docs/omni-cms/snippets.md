Snippets are a type of reusable content that insert predefined content into a page while editing. Using snippets for design elements ensures consistent styling across pages.

## Snippets List:

<ul>
{% for component in getComponents("ou-snippet") %}
  <li><a href="{{ ('/components/detail/' ~ component.handle) | path}}">{{ component.title }}</a></li>
{% endfor %}
</ul>
