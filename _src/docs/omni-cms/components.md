Components are a form-based type of reusable content that transform the information you enter into complex design elements. When a component is placed on a page, it acts as a form for you to enter information into.

Like MultiEdit, components restrict you to entering specific kinds of content, and the formatting is done "behind the scenes." This makes it easier for you to change the information on design elements with many moving parts, such as slide shows, info cards, and more.

## Components List:

<ul>
{% for component in getComponents("ou-component") %}
  <li><a href="{{ ('/components/detail/' ~ component.handle) | path}}">{{ component.title }}</a></li>
{% endfor %}
</ul>
