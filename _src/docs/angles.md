We use several variables in order to keep our angles a consistant 42º. Use the reference diagram below to determine which one to use.

<img src="https://assets.tcu.edu/cornerstone/images/tcu-angle-reference.png" alt="Angle Diagram" />

```scss
/// The Lead On angle based off a vertical starting position
/// @type Number
$lead-on-skew: -42deg;

/// The reversed version of $lead-on-skew
/// @type Number
$lead-on-skew-reversed: $lead-on-skew * -1;

/// The Lead On angle based off a horizontal starting position
/// @type Number
$lead-on-skew-alt: 48deg;

/// The reversed version of $lead-on-skew-alt
/// @type Number
$lead-on-skew-alt-reversed: $lead-on-skew-alt * -1;
```
