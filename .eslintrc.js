module.exports = {
  extends: ['eslint:recommended', 'prettier'], // extending recommended config and config derived from eslint-config-prettier
  parserOptions: {
    ecmaVersion: 2017,
  },
  rules: {
    eqeqeq: ['error', 'always'], // adding some custom ESLint rules
  },
  globals: {
    window: 'writable',
    document: 'writable',
    navigator: 'readonly',
    console: 'readonly',
  },
};
