# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- tagName, ariaLabel, target, and rel to button template

### Changed

- Moved button styles from Foundation to Cornerstone
- Update more templates to use button template render
- Title Card to use new button mixins

### Removed

- Legacy classes from buttons
- Foundation button settings

## [12.0.0] - 2025-01-08

### Added

- $hammerhead-divider-position and $hammerhead-divider-spacing

### Changed

- Change gulp, theo, and header files to mjs format.
- Update packages to latest versions.

## [11.0.0] - 2024-11-11

### Changed

- Updated foundation to v6.9.0

### Added

- $hammerhead-divider-align for changing the alignment of the hammerhead divider
- 1800/600 aspect ratio to hero image in large breakpoint

### Fixed

- Fix null query selector bug by add logic to tcu-sidebar-menu.js
- Fix Linting errors in tcu-sidebar-menu.js

## [10.2.1] - 2024-10-04

### Added

- "Region" landmark to alerts

### Removed

- fadeInDown class from alerts

### Fixed

- Removed extra endif in kitchen sink template

## [10.2.0] - 2024-08-14

### Changed

- Add aspect ratio to vimeo in video hero

## [10.1.0] - 2024-08-06

### Added

- Space to last sumenu item
- updateButtonDescription function in tcu-sidebar-menu.js
- toggleAriaAttributes function in tcu-sidebar-menu.js
- Check for current-page-item and current-page-parent in tcu-sidebar-submenu.js
- Make text bold through scss if item contains current-page-item class
- Sidebar-menu cypress test checks for current-page-item and current-page-parent classes

### Changed

- Moved the aria state to toggleAriaAttributes function in tcu-sidebar-menu.js

## [10.0.0] - 2024-07-22

### Added

- Site style switcher partial
- Bitbucket deployment for assets server
- Sidebar Component, styles, and js code.
- Animations scss file
- Timeline Component
- Sticky flag css file

### Changed

- Site style switcher styles
- Turned WordPress Patterns to Blocks
- Updated "in-progress" components to "complete"
- getComponent function checks for tags in variants
- Moved wordpress-block tag in components to a variant
- Moved settings imports to settings/settings.scss file
- Center Hero's video vertically if content is too big

### Removed

- WordPress Pattern tags
- Wordpress Pattern List Page
- Extra pseudo element for pull quote
- GPA Calculator styles
- Sticky flag from style.scss

## [9.1.0] - 2024-06-07

### Fixed

- Media card spacing and alignment

### Added

- Media Card styles

## [9.0.1] - 2024-05-28

### Fixed

- Title Card container breakpoints for pages with sidebar
- Publications axios request for template

## [9.0.0] - 2024-05-06

### Added

- Alternating video showcase alignment
- Set video showcase content width
- Background color to hollow buttons in Background Container
- Styles for anchor links that have no href
- Global focus styles
- Article footer
- Title card inner wrapper for container queries

## [8.0.0] - 2024-04-10

### Added

- Standard headcode and footcode includes
- Sticky Flags JS file
- Homepage with sticky flags layout
- Inline svg and css classes to sticky flags
- Sticky Flags cypress test
- WordPress Fractal Component View
- CSS custom properties for colors on :root

### Changed

- Sticky Flags use "has" instead of template logic for styling
- Fractal Preview uses storage event listener

### Removed

- $flags-background-colors variable from sticky flags
- $flags-icons variable from sticky flags

## [7.2.2] - 2024-03-20

### Changed

- Image gallery width when there is 4 or more pictures

## [7.2.1] - 2024-03-14

### Fixed

- Logic check for image gallery caption
- Clear elements within accordion content

## [7.2.0] - 2024-03-11

### Added

- Card Selector Component
- Two column variant of the Results Filter List
- Responsive Image Component
- Forms
- CSS comment snippets
- Wait time for image-gallery cypress test
- Margin below profile card set
- Line break for profile card emails
- Padding config for infographic ribbon
- nav-min-width CSS variable

### Changed

- Hero uses grid on large screens to deal with bigger captions
- Replace Foundation's button groups with custom version
- Clear blockquote from floats (clear fix)
- Profile Card Set to use CSS Grid instead of Foundation grid classes
- Placeholder images for image-gallery
- Profile card set example data
- Large local navigation wraps when necessary
- scripts.js uses nav-min-width to determine breakpoints for local navigation
- $local-navigation-mq-min-width to the main variable controlling navigation breakpoints

### Removed

- $local-navigation-mq-max-width from config.scss
- Image Left variant from hero

## [7.1.0] - 2024-01-31

### Added

- Text wrap balance to Site Name and Hammerheads
- Flex shrink to hero content buttons
- Inset Photo variation without caption
- Image Gallery Component

### Changed

- Hero to use gap instead of margin for its button group
- Hero content spacing
- Figcaption optional for Inset Photo
- Border for Inset Photo without caption
- Updated auto margins to use margin-inline
- Title Card Large Buttons cypress test to wait for vimeo to load

### Removed

- White text color from skip nav

## [7.0.0] - 2024-01-04

### Added

- Department hero modifier
- Margin bottom to hero when using mobile nav
- CSS to assets.tcu.edu build files
- Deprecated status
- Program Finder Form padding
- Component and full-width class to program finder form
- Default viewport sizes to linearClamp
- get-line-height default modifier
- Minimum font size tokens
- linearClamp to heading font sizes in \_typography.scss
- Heading margin config options
- Text wrap balance to headings

### Changed

- Root font size based on the body and browser default font size
- Breadcrumb to use body font size
- linearClamp to return single value if min and max are the sam
- Move font styles from Foundation \_settings.scss to \_typography.scss
- Homepage example to use program finder form
- Program Finder Form to use full width css instead of class

### Removed

- Hero fractal pre-college cornerstone classes

## [6.4.0] - 2023-12-11

### Changed

- Remove margin-inline from slider-on-small-only (if page contains side-bar & it's large)
- Blockquote to not use flexbox
- Status tag for the "Complete" label from "ready" to "complete"
- Majors and Programs component to deprecated status

## [6.3.0] - 2023-12-08

### Added

- Variables for tabcordions styling and config file
- Sidebar Feature content padding config
- Heading Divider scss mixin
- Warning when linearClamp might lead to less accessible results
- Blockquote component
- Cypress testing for Progam Finder Form and Blockquote
- Publications feed variables
- Generic Slider component
- Full width image helper class
- Sub Head styling and config file
- Announcements Feed variables
- xlarge variable in padding map for title card with large buttons
- Lead in paragraph variables

### Changed

- Margin around button in program finder form
- Program Finder Markup
- Sidebar Feature border supports custom styles
- Margin bottom for content wrapper
- Set Foundation blockquoute border to none
- Using div instead of aside element for callout and announcements feed
- Announcements feed no longer using header element
- Callout and Announcements Feed use h2 for heading
- Sub Head class used in callout is now called "tcu-sub-head".

### Removed

- Secondary class from button in publications feed twig file
- Modifier from Announcements Feed

## [6.2.0] - 2023-11-02

### Added

- Lazy loading attribute to iframes on Kitchen Sink page
- Accordion variables (accordion-header-background-active, accordion-arrow-filter, accordion-border, and accordion-border-radius)
- Hollow button config options
- Content button config options for svg spacing and border-radius
- Local Footer button style config options
- Homepage Hero style config options
- Fill and currentColor to stylelint strict value rules

### Changed

- Local Navigation home text and icon fills to default to text color
- $local-navigation-dropdown-border-color to be based off a scale of background color
- Fixed vertical alignment for accordion arrow
- Icon alignment on buttons
- Hollow button to make use of currentColor
- Local Footer to use hollow buttons

### Removed

- Specific button SVG fill colors

## [6.1.0] - 2023-10-11

### Added

- Border color for solid secondary buttons
- Arrow colors for content button
- Stylelint to package.json files array

### Changed

- Site name and dropdown list label text to text-black
- Pull Quote, Video Showcase, and Publication Listing color to text-black

### Removed

- References to tertiary

### Fixed

- Typo in font file
- SVG color for fact banner arrows

## [6.0.0] - 2023-10-05

### Changed

- Tokens and fonts to a default style
- Config and foundation/settings to default values
- Component variables to new defaults
- Rename announcements to announcements feed and add config options
- Change parallax component to hidden
- Color tokens to use neutral naming convention
- Remap tertiary color tokens to use neutral color tokens
- color-pick-contrast to use text-black and tolerance as needed
- $neutral-60 (previously $tertiary-1) to #767676

### Fixed

- Title card button video background
- Color settings for fact banner and multi column list
- Site style switcher render template name

## [5.3.0] - 2023-10-02

### Added

- Margin top spacing for FAQ updated text when in the bottom position
- Focus styling for video-showcase component
- Enable local use of college-cornerstone by changing dependencies and files in package.json
- Remote fonts to site style switcher
- Typekit fonts for Harris College
- Global context for the head and body element

### Changed

- Updated fast-xml-parser, cssnano, cypress, eslint-config-prettier, plop, and prettier
- Formatted files with Prettier v3.0.3
- XMLParser in Cypress test files for Article Listing and Publications Feed
- Site names are wrapped in divs instead of heading elements for accessibility
- foundation-sites to v6.8.1
- Moved site style switcher script to twig partials

### Fixed

- getComponent fractal function using correct links for default components

## [5.2.0] - 2023-08-31

### Changed

- Disable cache when intercepting vimeo player js during cypress tests
- Manually display links from "a" elements, hide nav, when printing
- Pinned gpa calculator wrapper is contained when header is not fixed
- FAQ app styles to work better with college cornerstone

### Added

- Specific Firefox print settings
- Variables and comments FAQ app styles

## [5.1.0] - 2023-07-28

### Added

- Accessible text to dropdown list
- equal-height class to \_layout.scss
- gpa-calculator scss file
- Accreditation link to global footer
- Announcements styling file
- Announcements Fractal Component
- Announcements Cypress test

### Changed

- Site links in global footer
- Global nav has title attributes, new media link
- Button fits content on title cards when on large screen

## [5.0.0] - 2023-07-11

### Added

- .nowrap utility class
- Wordpress page with blocks and patterns
- Wordpress tags and labels for blocks and patterns
- Stylelint in vs code recommendations and settings
- Tabcordions js functions, removeRoles(), addRoles();
- FAQ styles
- Vue app tags
- Scrollable tables
- Print scss partial

### Changed

- Local symlink documentation
- Article Cards' tags change to "ul" instead of div, added style changes to article-cards.
- getComponent function in twig.js checks for tags in variants
- Update Stylelint
- Started using Stylelint's API instead of gulp-stylelint
- Linting scss files has its own gulp task
- Styles scss path corrected in the Gulpfile
- Tabcordions aria-expanded is on the button element
- Tabcordion js & scss file uses aria-expanded instead of aria-selected attribute for visible class, aria-selected removed when on mobile
- Add Delay for Cypress tests on alert components
- Zebra striping for responsive tables
- Table documentation
- Pull-quote uses figure, blockquote, and figcaption
- Directory profile image sizes
- Accordion content visible on print

## [4.0.2] - 2023-05-15

### Added

- Side nav multi-columns set to 1fr for large breakpoint.

## [4.0.1] - 2023-05-03

### Added

- Default line height to Accordion Header

### Changed

- Move setValues() for accordion header

## [4.0.0] - 2023-05-01

### Changed

- Local Footer to use grid instead of flexbox
- Logo sizes in Local Footer
- Local Footer can use variable number of columns

### Removed

- Grid classes in Local Footer

## [3.4.0] - 2023-04-25

### Added

- Site Header block to twig page layout
- Department page layout
- Dark and Light backgrounds for Multi Column List

### Changed

- $image-flair and $image-flair-alt to false by default
- Two Column List to Multi Column List
- Multi Column List now uses grid instead of flexbox

## [3.3.0] - 2023-03-29

### Added

- aria-label to button template
- Default color to Accordion headers
- Style to remove margins from nested tcu-component classes for flexbox containers

### Changed

- Pull Quote, Title Card, and Video Showcase to use button template
- Prettier to ignore twig files

## [3.2.0] - 2023-03-17

### Added

- Publications Feed
- Publications Listing

## [3.1.0] - 2023-02-08

### Added

- Image width and height config variables for the Image Card and Article Card
- Suggested image sizes to the README of hero, image card, profile card, pull quote, and article card
- Suggested image sizes and new aspect ratios to Title Card Image Left and Right
- Max width to article cards in article listing grid on large screen sizes

### Changed

- Hammerhead divider color to currentcolor by default
- $title-card-three-button-background-color to $title-card-three-button-background in config.scss

### Fixed

- scripts.js to initialize slide menu JS even when there are no submenus

## [3.0.1] - 2022-01-05

### Changed

- cornerTriangle mixin to always use lead on angle
- cornerTriangle mixin to offer the choice between background and border properties

## [3.0.0] - 2022-12-15

### Added

- honors stylesheet
- linearClamp sass function
- Video support to hero component
- Font variables for image card title and description
- Font variables for localist widget event dates
- Max width and svg size variables for pull quote
- Support for .aligncenter, .alignleft, and .alignright classes
- Two Column List
- Video Showcase
- Separator
- Full width variant of inset photo
- Config options for inset photo caption font
- JS files for Cornerstone components
- vscode settings and extension files
- button type configuration for title card large buttons
- Program Finder Form component

### Changed

- Add JS intercept to Title Card Large Buttons test
- Refactor Cypress tests to move action steps to end of chains
- $localist-widget-event-divider set to false will produce a single column event list
- Renamed tcu.accordions.js to tcu-accordions.js
- Components that use scripts will now use the local version instead of the one on assets.tcu.edu

### Fixed

- News feed url
- Column count for archive list on small screens
- tcu-accordions.js checks for faqsearch classes before initializing

## [2.3.2] - 2022-10-26

### Fixed

- News rss url

## [2.3.1] - 2022-09-27

### Added

- Hammerhead styles in its own scss file

### Removed

- Hammerhead styles from typography.scss

## [2.3.0] - 2022-04-14

### Added

- Lead On angle variables
- Documentation page for Lead On angles

### Changed

- cornerTriangle mixin to use lead on angle
- Fact Slider, Content Button, and Localist Widget to use lead on angles

### Removed

- safari 8, IE11 from json browser list

## [2.2.2] - 2021-12-20

### Added

- Preview size to fractal theme

### Changed

- Background container gradient to stop 20% from bottom of container

### Fixed

- Changed the tabcordion colors to align with the default styling
- Card width on card variant of the article listing

### Removed

- z-index from dropdown list that was causing overlap with the main menu

## [2.2.1] - 2021-12-07

### Added

- Configuration for small profile text

## [2.2.0] - 2021-12-06

### Added

- Min/max width media queries as variables for local navigation
- Sticky flags

### Fixed

- Set flex-direction on wide article cards to row instead of row-reverse

## [2.1.0] - 2021-11-18

### Added

- Specific values for the skip nav colors for consistency
- Article image
- Directory and tcu button classes to shame file

### Fixed

- Directory width when using old tcu-article classes

## [2.0.0] - 2021-11-15

### Added

- Button letter spacing and text transform config options
- Images to config options
- Margin and font styles to accordion config options
- Font styles to sidebar feature config options
- Sidebar feature defaults and optional border
- Margin and font styles to accordion config options
- Conditional for callout button hover
- Font styles to sidebar feature config options
- Background container fonts
- Inset photo config options
- Pull quote config options
- Question variation of accordions
- Tabcordion config and more basic default styles for tabcordions
- Default hero styles
- Full width mixin
- Fact Slider
- Refactor tag
- Button alignment for sidebar feature
- Accordion header padding config options
- Margin between checklist svg and text for accordion headers
- Heading style with a divider
- Sidebar List component
- Hammerhead divider color, height, and width config
- Article listing components
- Article cards components
- Pagination component
- Blog related layouts
- Fractal theme switcher
- Article component
- Dropdown List component
- Link List component
- Wrapping element around page heading

### Changed

- Local navigation defaults to match cornerstone-source sites
- Local footer defaults to match cornerstone-source sites
- Back to top defaults to be more in line with cornerstone-source sites
- Updated config from coe
- Moving pull quote grid out of markup and into css
- Spacing and button/divider colors for title card
- Title card three button padding on small screens
- Position of ou form checkbox and radio buttons
- Moved global variables to variables.scss. Can now be overrided in config.scss
- Moved template context data to using meta props
- Move <main> tag to include more of the main page content
- Removed modernizr class for clip path and changed to an @supports query

### Removed

- Old classes from title card
- Top margin of first item in sidebar
- Removed rem-calc from $component-max-width

### Fixed

- Add conditional for call out button hover when the button isn't hollow
- Content Button defaults
- Title Card defaults
- Sass compilation speed
- Update content button defaults
- Title card defaults
- Properly applied $image-card-set-background

## [1.0.0] - 2021-09-15

### Changed

- Renamed OmniUpdate docs to Omni CMS
- Updated autoprefixer, cssnano, cypress, gulp-postcss, gulp-sass, http-server,
  prettier, start-server-and-test, stylelint, stylelint-declaration-strict-value versions
- Updated to use dart sass
